<?php
namespace MiniBC\addons\momconnector\controllers;

use Bigcommerce\Api\Client;

use MiniBC\addons\momconnector\entities\Transaction;
use MiniBC\addons\momconnector\services\MOMService;
use MiniBC\addons\momconnector\services\PaymentService;

use MiniBC\addons\recurring\gateways\AuthorizeNet;
use MiniBC\addons\recurring\services\PaymentService as RecurringPayment;

use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;
use MiniBC\core\EntityFactory;

/**
 * Process requests for M.O.M. Connector
 *
 * @uses MiniBC\core\Auth
 * @uses MiniBC\core\EntityFactory
 * @uses MiniBC\core\connection\ConnectionManager
 * @uses MiniBC\core\entities\Addon
 */
class AddonController
{
	/** @var Store $store */
	private $store = null;

	/** @var Addon $addon */
	private $addon = null;

	/**
	 * assign the current addon
	 *
	 * @param Addon $addon
	 */
	public function setAddon(Addon $addon)
	{
		$this->addon = $addon;
	}

	/**
	 * get updated orders from M.O.M. server
	 *
	 * @param array $args
	 */
	public function updateFromMOM($args = array())
	{
		$response = MOMService::getInstance()->getMOMOrders($this->store);

		if ($response->success) {
			http_response_code(200);
			echo json_encode(array( 'success' => true, 'orders' => $response->orders ));
			exit;
		}

		http_response_code(500);
		echo json_encode(array( 'message' => $response->message ));
		exit;
	}

	/**
	 * process payment for a batch of orders
	 *
	 * @param array $args
	 * @throws \Exception
	 * @throws \MiniBC\core\entities\exception\UnknownEntityException
	 */
	public function processBatch($args = array())
	{
		$orders = $_POST['orders'];

		$response = array( 'result' => array() );

		$customer = Auth::getInstance()->getCustomer();
		$store = $customer->stores[0];
		$paymentService = PaymentService::getInstance();



		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');

		foreach ($orders as $order) {
			$amount = (float)$order['amount'];
			$orderId = (int)$order['order_id'];

			$orderResult = array(
				'success' 		=> false,
				'message' 		=> '',
				'id'			=> $orderId,
				'amount'		=> $amount
			);

			if ($amount <= 0) {
				// invalid request, amount is less than 0
				$orderResult['message'] = 'Please enter a valid amount.';
				$response['result'][] = $orderResult;

				continue;
			}

			$result = $paymentService->createPayment($orderId, $amount, $store);

			if ($result['success']) {
				$orderResult['success'] = true;
				$orderResult['message'] = 'Successful.';
				$response['result'][] = $orderResult;

				// the SV amount is always paid with the first payment
				$db->update(
					'mom_orders',
					array( 'sv_paid' => 1 ),
					array( 'customer_id' => $this->store->id, 'bc_order_id' => $orderId)
				);

				continue;
			}

			$orderResult['message'] = $result['message'];
			$response['result'][] = $orderResult;
		}

		echo json_encode($response);
		exit;
	}

	/**
	 * process payment for a single order
	 *
	 * @param array $args
	 * @throws \Exception
	 * @throws \MiniBC\core\entities\exception\UnknownEntityException
	 */
	public function processPayment($args = array())
	{
		$data = $_POST;

		if (empty($data['amount'])) {
			// missing payment amount from request
			http_response_code(400);
			exit;
		}

		$amount = (float)$data['amount'];
		$orderId = (int)$data['order_id'];
		$customer = Auth::getInstance()->getCustomer();
		$store = $customer->stores[0];

		if ($amount <= 0) {
			// invalid request, amount is less than 0
			http_response_code(400);
			exit;
		}

		$paymentService = PaymentService::getInstance();
		$result = $paymentService->createPayment($orderId, $amount, $store);

		// payment successful
		if ($result['success']) {
			http_response_code(200);
			echo json_encode(array( 'success' => true ));

			exit;
		}

		// display error
		http_response_code($result['http_code']);
		if (!empty($result['message'])) {
			echo json_encode(array('message' => $result['message']));
		}

		exit;
	}

	public function getBatchRefunds($args = array())
	{
		$response = MOMService::getInstance()->getMOMRefunds($this->store);

		if ($response->success) {
			http_response_code(200);
			echo json_encode(array( 'success' => true, 'orders' => $response->refunds ));
			exit;
		}

		http_response_code(500);
		echo json_encode(array( 'message' => $response->message ));
		exit;
	}

	public function processBatchRefund($args = array())
	{
		$orders = $_POST['orders'];

		$response = array( 'result' => array() );

		$customer = Auth::getInstance()->getCustomer();
		$store = $customer->stores[0];
		$paymentService = PaymentService::getInstance();

		foreach ($orders as $order) {
			$amount = (float)$order['amount'];
			$orderId = (int)$order['order_id'];
			$items = $order['items'];

			$orderResult = array(
				'success' 		=> false,
				'message' 		=> '',
				'id'			=> $orderId,
				'amount'		=> $amount
			);

			if ($amount <= 0) {
				// invalid request, amount is less than 0
				$orderResult['message'] = 'Please enter a valid amount.';
				$response['result'][] = $orderResult;

				continue;
			}

			$result = $paymentService->refundPayment($orderId, $amount, 'return', $items, $store);

			if ($result['success']) {
				$orderResult['success'] = true;
				$orderResult['message'] = 'Successful.';
				$response['result'][] = $orderResult;

				continue;
			}

			$orderResult['message'] = $result['message'];
			$response['result'][] = $orderResult;
		}

		echo json_encode($response);
		exit;
	}

	/**
	 * process a refund for a previous transaction
	 *
	 * @param array $args
	 * @throws \MiniBC\core\entities\exception\UnknownEntityException
	 * @throws \MiniBC\core\entities\exception\UnsupportedPopulationMethodException
	 */
	public function refundPayment($args = array())
	{
		$data = $_POST;

		if (empty($data['amount'])) {
			// missing refund amount from request
			http_response_code(400);
			exit;
		}

		$amount = (float)$data['amount'];
		$orderId = (int)$data['order_id'];
		$customer = Auth::getInstance()->getCustomer();
		$store = $customer->stores[0];

		if ($amount <= 0) {
			// invalid request, amount is less than 0
			http_response_code(400);
			exit;
		}

		$paymentService = PaymentService::getInstance();
		$result = $paymentService->refundPayment($orderId, $amount, 'refund', array(), $store);

		// payment successful
		if ($result['success']) {
			http_response_code(200);
			echo json_encode(array( 'success' => true ));

			exit;
		}

		// display error
		http_response_code($result['http_code']);
		if (!empty($result['message'])) {
			echo json_encode(array('message' => $result['message']));
		}

		exit;
	}

	public function getSettings($args = array())
	{
		$response = array(
			'minfraud_license' 	=> '',
			'api_endpoint'	=> ''
		);

		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$result = $db->selectFirst('mom_settings', array( 'customer_id' => $this->store->id ));

		if (!empty($result)) {
			$response['minfraud_license'] = $result['minfraud_license'];
			$response['api_endpoint'] = $result['api_endpoint'];
		}

		echo json_encode($response);
		exit;
	}

	public function saveSettings($args = array())
	{
		$data = $_POST;
		$response = array( 'success' => true );

		$insertData = array();
		if (isset($data['api_endpoint'])) {
			$insertData['api_endpoint'] = $data['api_endpoint'];
		}

		if (isset($data['license_key'])) {
			$insertData['minfraud_license'] = $data['license_key'];
		}

		/** @var MySQLConnection $db */
		try {
			$db = ConnectionManager::getInstance('mysql');
			$exists = $db->selectFirst('mom_settings', array( 'customer_id' => $this->store->id ));

			if (empty($exists)) {
				$insertData['customer_id'] = $this->store->id;
				$db->insert($insertData);
			} else {
				$db->update('mom_settings', $insertData, array( 'customer_id' => $this->store->id ));
			}

			http_response_code(200);
			echo json_encode($response);
			exit;
		} catch (\Exception $e) {
			http_response_code(500);
			$response['success'] = false;

			echo json_encode($response);
			exit;
		}
	}

	/**
	 * assigns the current store for the controller
	 *
	 * @param Store $store
	 */
	public function setStore(Store $store)
	{
		$this->store = $store;
	}

	/**
	 * returns information on the current addon
	 */
	public function showAppInfo()
	{
		echo json_encode(array('app' => $this->addon->getDisplayDataAsArray()));
		exit;
	}
}