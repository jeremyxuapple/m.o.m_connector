<?php
namespace MiniBC\addons\momconnector\controllers;

use MiniBC\addons\momconnector\entities\Order;

use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;
use MiniBC\core\EntityFactory;

class OrdersController
{
	const ORDERS_PER_PAGE = 10;

	/** @var Addon $addon */
	protected $addon;

	/** @var Store $store */
	protected $store;

	/** @var array searchable columns */
	protected $searchFields = array(
		'bc_order_id', 'mom_order_id', 'first_name', 'last_name', 'email', 'cc_last4'
	);

	/**
	 * retrieves a specific order
	 *
	 * @param array $args
	 */
	public function getOrder($args = array())
	{
		$orderId = $args['id'];

		/** @var Order $order */
		$order = EntityFactory::makeFromId('Order', $orderId, 'momconnector');

		if ($order->id <= 0) {
			// order not found
			http_response_code(404);
			exit;
		}

		$response = $order->formatForJSONAPI();

		if (isset($response['data'][0])) {
			$response['data'] = $response['data'][0];
		}

		echo json_encode($response);

		exit;
	}

	/**
	 * retrieves a list of orders
	 *
	 * @param array $args
	 */
	public function getOrders($args = array())
	{
		$data = $_GET;

		// get pagination
		$page = 1;
		$offset = 0;
		$limit = self::ORDERS_PER_PAGE;

		if (!empty($data['page']) && (int)$data['page'] > 0) {
			$page = (int)$data['page'];
			$offset = ($page - 1) * $limit;
		}

		// determine if this is a search
		$isSearch = false;

		if (!empty($data['term']) && trim($data['term']) != '') {
			$isSearch = true;
		}

		$response = array(
			'data' 		=> array(),
			'included' 	=> array(),
			'meta' 		=> array( 'pages' => 0 )
		);

		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');

		// get the number of orders
		if ($isSearch) {
			$searchQuery = $this->buildSearchQuery($data['term']);
			$count = $this->getOrderCount($searchQuery['where'], $searchQuery['vars']);
		} else {
			$count = $this->getOrderCount('1 = 1', array( ':customer_id' => $this->store->id ));
		}

		// no orders found, return results
		if ($count <= 0) {
			echo json_encode($response);
			exit;
		}

		// number of pages
		$response['meta']['pages'] = ceil($count / $limit);

		if ($isSearch) {
			$searchQuery = $this->buildSearchQuery($data['term']);

			$query = sprintf(
				'
				SELECT `bc_order_id`, ( %s ) AS score
				FROM `mom_orders`
				WHERE ( %s ) AND `customer_id` = :customer_id
				ORDER BY score DESC
				LIMIT %d, %d',
				$searchQuery['score'],
				$searchQuery['where'],
				$offset,
				$limit
			);

			$orders = $db->query(
				$query,
				$searchQuery['vars']
			);
		} else {
			$orders = $db->select(
				'mom_orders',
				array( 'customer_id' => $this->store->id ),
				$limit,
				$offset,
				array( 'bc_order_id' => 'DESC' )
			);
		}

		if (empty($orders)) {
			echo json_encode($response);
			exit;
		}

		foreach ($orders as $orderData) {
			/** @var Order $order */
			$order = EntityFactory::makeFromId('Order', $orderData['bc_order_id'], 'momconnector');
			$order->setStore($this->store);

			$apiData = $order->formatForJSONAPI(false);

			$response['data'] = array_merge($response['data'], $apiData['data']);
			$response['included'] = array_merge($response['included'], $apiData['included']);
		}

		echo json_encode($response);
		exit;
	}

	public function getOrderCount($where, $vars)
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');

		$countQuery = sprintf(
			'
				SELECT COUNT(`bc_order_id`) AS count
				FROM `mom_orders`
				WHERE ( %s ) AND `customer_id` = :customer_id
				',
			$where
		);

		$countResult = $db->queryFirst($countQuery, $vars);

		if (!empty($countResult)) {
			return (int)$countResult['count'];
		}

		return 0;
	}

	/**
	 * build search query based on search fields
	 *
	 * @param string $term search term
	 * @return array
	 */
	public function buildSearchQuery($term)
	{
		$score = array();
		$where = array();

		foreach ($this->searchFields as $field) {
			$score[] = sprintf('IF (`%s` = :term, 100, 0)', $field);
			$score[] = sprintf('IF (`%s` LIKE :wildcard, 20, 0)', $field);
			$where[] = sprintf('`%s` LIKE :wildcard', $field);
		}

		return array(
			'score' => implode( ' + ', $score ),
			'where' => implode( ' OR ', $where ),
			'vars'	=> array(
				':customer_id' => $this->store->id,
				':term'	=> $term,
				':wildcard' => '%' . $term . '%'
			)
		);
	}

	public function setStore(Store $store)
	{
		$this->store = $store;
	}

	public function setAddon(Addon $addon)
	{
		$this->addon = $addon;
	}
}