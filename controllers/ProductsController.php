<?php

namespace MiniBC\addons\momconnector\controllers;

use Bigcommerce\Api\Client;

use MiniBC\addons\momconnector\entities\Order;
use MiniBC\addons\momconnector\services\MOMService;
use MiniBC\addons\momconnector\services\ProductService;

use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;
use MiniBC\core\EntityFactory;

class ProductsController
{	
	public function __construct()
	{   
	    
		$this->customer->id = 20;
		$this->store = EntityFactory::makeFromId('Store', $this->customer->id);

	    // $this->customer = Auth::getInstance()->getCustomer();
	    // $this->store = $this->customer->stores[0];

	    $this->productService = ProductService::getInstance();
	    $this->momService = MOMService::getInstance();
	    $this->db = ConnectionManager::getInstance('mysql');
	}

	public function getMOMProducts()
	{ 	
	  	$products = $this->momService->getMOMProducts($this->store);
	  	$products = json_decode($products);

	  	// $p = array();
	  	// $i = 1;
	  	// while ( $i < sizeof($p_array)) {
	  	// 	$s = json_decode(json_encode($p_array[$i]), true);
	  	// 	array_push($p, $s);
	  	// 	$i ++;
	  	// }
	  	$product_array = array();
	  	foreach($products as $p) {
	  		$product = json_decode(json_encode($p), true);
	  		array_push($product_array, $product);
	  	}

	  	// print_r($product_array);
	  	// exit();
	  	
	   	return $product_array;
	}

	/**
	*	Recevie the data from M.O.M server and assemble SKUs array for comparasion and assign to tasks to different 
	* subordinated functions accordingly
	*	
	*/

	public function productsManager()
	{	
		$api = $this->store->getApiConnection();
		$customer_id = $this->customer->id;
		
		$BCProducts = $this->db->query('
										SELECT * 
										FROM bigbackup_bc_products
										WHERE customer_id = :customer_id
										', 
										array(
											'customer_id' => $customer_id
										));

		if ( $BCProducts != null ) {
			foreach ($BCProducts as $bcProduct) {
				$bcSkus[] = $bcProduct['sku'];
			}
		} else {
			$bcSkus = array();
		}

		$MOMProducts = $this->getMOMProducts();

		foreach ($MOMProducts as $momProduct) {
			$momSkus[] = $momProduct['number'];
		}

		// Pass the data to MOMProductFormatter if the product needs to be created or to be update, receive returned  data and then do actions
		$allSkus = array_unique(array_merge($momSkus,$bcSkus));
		$createSkus = array_diff($momSkus, $bcSkus);
		$deleteSkus = array_diff($bcSkus, $momSkus);

		// Combine two skus array from BigCommerce and MOMServer, and then elminate the $create and $delete array will get back the skus which are needed for update
		$updateSkus = array_diff(array_diff($allSkus, $createSkus), $deleteSkus);

	  // Pass the data to createProduct if the sku doesn't exist in the BCProducts but in MOMProducts
		if ( $createSkus != null) {
			
			$productsForCreation = $this->productService->selectMOMProducts($createSkus, $MOMProducts);

			foreach ($productsForCreation as $product) {
				$formattedProduct = $this->productService->dataFormatter($product);
				$this->createProduct($formattedProduct);
			}
		}

		// Pass the data to deleteProducts if the sku exist in the BCProducts but not in MOMProducts
		if ( $deleteSkus ) {
		
			$this->deleteProducts($BCProducts, $deleteSkus, $api);

		}

		// Pass the data to updateProduct the sku exist in both BCProducts and MOMProducts
		if ( $updateSkus != null ) {

			$productsForUpdate = $this->productService->selectMOMProducts($updateSkus, $MOMProducts);

			foreach ($productsForUpdate as $product) {
				$updateProducts = $this->productService->assembleUpdateProducts($updateSkus, $MOMProducts, $BCProducts);
				$this->updateProducts($updateProducts);
			}
		}

		exit('success');
	}

	/**
	* Create Products
	* @param - $product: the formatted data ready to push into the BigCommerce Store
	*/

	public function createProduct($formattedProduct)
	{	
		$api = $this->store->getApiConnection();
		extract($formattedProduct);
		// print_r($formattedProduct);
		// exit();
		$newProduct = $api::createProduct($product);

		$error = $api::getLastError();
		if ( $error != null ) {
			echo 'error creating...';
			print_r($error);
			exit();
		}

		$productId = $newProduct->id;
		
		// IMAGE
		if ( isset($images) ) {	
			// foreach($images as $key => $url) {
			// 	$api::createProductImage($productId, array(
			// 		'image_file' => $url
			// 	));
			// }
			$api::createProductImage($productId, array(
					'image_file' => $images
				));

		}


		// BULK PRICING
		if ( isset($bulkPricing) ) {
			$api::createProductBulkPricingRules($productId, $bulkPricing);	
		}

		// CUSOM FIELDS
		if ( isset($customFields) ) {
			foreach ($customFields as $name => $text) {
				$api::createProductCustomField($productId, array(
					'name' => $name,
					'text' => $text
				));	
			}
		} // End of the customFields if statement
	}
	
	/**
	*	Update Products
	*
	*/ 
	public function updateProducts($updateProducts)
	{	
		$api = $this->store->getApiConnection();

		foreach ($updateProducts as $p) {

			$momProduct = $p['momProduct'];
			$bcProduct = $p['bcProduct'];
			$product_id = $bcProduct['bc_id'];

			print_r($p['momProduct']);

			exit();
			
			if ( isset($momProduct['images']) ) {
				echo "Updating images... \n";
				$momImages = $momProduct['images'];
				$this->productService->processImages($product_id, $momImages = null, $api);
			}


			// if ( isset($momProduct['bulkPricing'])) ) {
				echo "Updating bulkPricing... \n";
				// $momBulkPricing = $momProduct['bulkPricing'];

				// $momBulkPricing = array(
				// 	'type' => 'percent',
				// 	'min' => 50,
				// 	'type_value' => 5
				// 	);
				$this->productService->processBulkPricing($product_id, $momBulkPricing = null, $api);
			// }

			// if ( isset($momProduct['customFields']) ) {
			// 	echo "Updating customFields... \n";
			// 	$momCustomFields = $momProduct['customFields'];
			// 	$this->productService->processCustomFields($product_id, $momCustomFields, $api);
			// }

			// $momProductBasicInfo = $momProduct['product'];
			// $this->productService->processBasicProductInfo($bcProduct, $momProductBasicInfo, $api);

			exit('1st product updating done...');
		}

	}

	public function deleteProducts($BCProducts, $deleteSkus, $api)
	{
		foreach ($deleteSkus as $sku) {
			foreach ($BCProducts as $p) {
				if ($p['sku'] == $sku) {
					$ids[] = $p['bc_id'];
					break;
				}
			}
		}

		foreach ($ids as $id) {
			$api::deleteProduct($id);
		}

	}

	/**
	* Reading CSV file from the M.O.M Server
	*/
	public function readCSVFile()
	{
		$result = array();

		if (($handle = fopen(__DIR__ ."/STOCK_TABLE.csv", "r")) !== FALSE) {

	    $column_headers = fgetcsv($handle); // read the row.

	    while (($data = fgetcsv($handle)) !== FALSE) {
	    		
	    		$record = array_combine($column_headers, $data);
	        $i = 0;
	        array_push($result, $record);
	    }
	    fclose($handle);
		}

		// print_r(sizeof($result));
		print_r($result);

	}


	public function setStore(Store $store)
	{
		$this->store = $store;
	}

	public function setAddon(Addon $addon)
	{
		$this->addon = $addon;
	}

}