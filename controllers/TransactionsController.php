<?php
namespace MiniBC\addons\momconnector\controllers;

use MiniBC\addons\momconnector\services\TransactionService;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;

class TransactionsController
{
	/** @var Store|null $store current store instance */
	protected $store;

	/** @var  Addon|null $addon addon instance */
	protected $addon;

	/** @var array valid filter types */
	protected $validTypes = array( 'all', 'payment', 'refund', 'error' );

	protected $dateFormat = 'Y-m-d';

	/**
	 * print transaction based on a set of parameters
	 *
	 * @param array $args
	 */
	public function printTransactions($args = array())
	{
		// parse parameters
		$data = $_GET;

		// assign defaults
		$type = 'all';
		$page = 1;

		// set timezone to PST
		$timezone = new \DateTimeZone('PST');

		$now = new \DateTime();
		$startDate = new \DateTime();
		$startDate->setTimezone($timezone)->setTime(0, 0, 0);
		$endDate = $now->add(new \DateInterval('P1D'));

		// filter by transaction type
		if (!empty($data['type']) && in_array($data['type'], $this->validTypes)) {
			$type = $data['type'];
		}

		// filter by date
		if (!empty($data['date'])) {
			$start = \DateTime::createFromFormat($this->dateFormat, $data['date']);

			if (!$start) {
				http_response_code(400);
				exit;
			}

			$start->setTimezone($timezone)->setTime(0, 0, 0);

			$end = \DateTime::createFromFormat($this->dateFormat, $data['date']);
			$end->setTimezone($timezone)->setTime(0, 0, 0)->add(new \DateInterval('P1D'));

			$startDate = $start;
			$endDate = $end;
		}

		$service = new TransactionService();

		$count = $service->getTransactionCount($type, $startDate, $endDate, $this->store);

		if ($count <= 0) {
			// no results
			echo 'No transactions found.';
			exit;
		}

		$transactions = $service->getTransactions('all', $type, $startDate, $endDate, $this->store);
		include(APP_ROOT . '/addons/momconnector/print_templates/transactions.php');
	}

	/**
	 * get transactions based on a set of parameters
	 *
	 * @param array $args
	 */
	public function getTransactions($args = array())
	{
		// parse parameters
		$data = $_GET;

		// assign defaults
		$type = 'all';
		$page = 1;

		// set timezone to PST
		$timezone = new \DateTimeZone('PST');

		$now = new \DateTime();
		$startDate = new \DateTime();
		$startDate->setTimezone($timezone)->setTime(0, 0, 0);
		$endDate = $now->add(new \DateInterval('P1D'));

		// filter by transaction type
		if (!empty($data['type']) && in_array($data['type'], $this->validTypes)) {
			$type = $data['type'];
		}

		// filter by date
		if (!empty($data['date'])) {
			$start = \DateTime::createFromFormat($this->dateFormat, $data['date']);

			if (!$start) {
				http_response_code(400);
				exit;
			}

			$start->setTimezone($timezone)->setTime(0, 0, 0);

			$end = \DateTime::createFromFormat($this->dateFormat, $data['date']);
			$end->setTimezone($timezone)->setTime(0, 0, 0)->add(new \DateInterval('P1D'));

			$startDate = $start;
			$endDate = $end;
		}

		// pagination
		if (!empty($data['page']) && (int)$data['page'] > 0) {
			$page = (int)$data['page'];
		}

		// response data
		$response = array(
			'data' 		=> array(),
			'included' 	=> array(),
			'meta' 		=> array( 'pages' => 0 )
		);

		$service = new TransactionService();

		$count = $service->getTransactionCount($type, $startDate, $endDate, $this->store);

		if ($count <= 0) {
			// no results
			echo json_encode($response);
			exit;
		}

		$maxPages = ceil($count / $service::RESULTS_PER_PAGE);
		$response['meta']['pages'] = $maxPages;

		if ($maxPages < $page) {
			// requested page is greater than the number of pages available
			echo json_encode($response);
			exit;
		}

		$transactions = $service->getTransactions($page, $type, $startDate, $endDate, $this->store);

		if (empty($transactions)) {
			echo json_encode($response);
			exit;
		}

		foreach ($transactions as $transaction) {
			$apiData = $transaction->formatForJSONAPI(false);

			$response['data'] = array_merge($response['data'], $apiData['data']);
			$response['included'] = array_merge($response['included'], $apiData['included']);
		}

		echo json_encode($response);
		exit;
	}

	/**
	 * assign addon instance
	 *
	 * @param Addon $addon
	 */
	public function setAddon(Addon $addon)
	{
		$this->addon = $addon;
	}

	/**
	 * assign current store
	 *
	 * @param Store $store
	 */
	public function setStore(Store $store)
	{
		$this->store = $store;
	}
}