<?php
namespace MiniBC\addons\momconnector;

use MiniBC\addons\momconnector\controllers\AddonController;
use MiniBC\addons\momconnector\controllers\OrdersController;

use MiniBC\addons\momconnector\controllers\TransactionsController;
use MiniBC\core\Auth;
use MiniBC\core\Config;
use MiniBC\core\controller\ControllerManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\entities\Store;
use MiniBC\core\route\Route;

use Firebase\JWT\JWT;

/**
 * Router for M.O.M. Connector
 *
 * @uses MiniBC\core\Config
 * @uses MiniBC\core\controller\ControllerManager
 * @uses MiniBC\core\entities\Addon
 * @uses MiniBC\core\route\Route
 */
class AddonRoute extends Route
{
	/** @var string $basePath base addon URI */
    public $basePath = '';

	/** @var string $customerPath base addon URI for customers */
	public $customerPath = '';

	/** @var string $name addon name */
    public $name = '';

	/** @var Addon $addon */
    public $addon = null;

    /**
     * setup routes for this addon
	 *
     * @param 	Addon 	$addon 	instance of the addon object
     */
    public function __construct(Addon $addon)
		{		
      $customer = Auth::getInstance()->getCustomer();
			$store = $customer->stores[0];

      $this->name = $addon->name;
      $this->addon = $addon;

      $customerBasePath = Config::get('routes::customer');
      $addonBasePath = Config::get('routes::addon');

      $this->basePath = $addonBasePath . '/' . $this->name;
      $this->customerPath = $customerBasePath . $this->basePath;

      // dashboard routes
			$this->setOrdersRoute($store);
			$this->setTransactionsRoute($store);
			$this->setProductsRoute($store);
			$this->setAddonRoute($store);
    }

	/**
	 * assign general routes for an addon
	 *
	 * @param Store $store
	 */
	protected function setAddonRoute(Store $store)
	{
		/** @var AddonController $controller */
		$controller = ControllerManager::get('Addon@' . $this->name);
		$controller->setAddon($this->addon);
		$controller->setStore($store);


		$this->get($this->customerPath . '/update/refunds', array($controller, 'getBatchRefunds'));
		// $this->get($this->customerPath . '/update', array($controller, 'updateFromMOM'));
		$this->get($this->customerPath . '/update', array($controller, 'getProducts'));

		$this->post($this->customerPath . '/payments/batch/refunds', array($controller, 'processBatchRefund'));
		$this->post($this->customerPath . '/payments/batch', array($controller, 'processBatch'));
		$this->post($this->customerPath . '/payments', array($controller, 'processPayment'));
		$this->post($this->customerPath . '/refunds', array($controller, 'refundPayment'));
		$this->get($this->customerPath . '/settings', array($controller, 'getSettings'));
		$this->post($this->customerPath . '/settings', array($controller, 'saveSettings'));
	}

	protected function setTransactionsRoute(Store $store)
	{
		/** @var TransactionsController $controller */
		$controller = ControllerManager::get('Transactions@' . $this->name);
		$controller->setAddon($this->addon);
		$controller->setStore($store);

		$this->get($this->customerPath . '/reports/print', array($controller, 'printTransactions'));
		$this->get($this->customerPath . '/mom-transactions', array($controller, 'getTransactions'));
	}

	/**
	 * assign order resource routes
	 *
	 * @param Store $store
	 */
	protected function setOrdersRoute(Store $store)
	{
		/** @var OrdersController $controller */
		$controller = ControllerManager::get('Orders@' . $this->name);
		$controller->setAddon($this->addon);
		$controller->setStore($store);

		$this->get($this->customerPath . '/mom-orders/{id:.+}', array($controller, 'getOrder'));
		$this->get($this->customerPath . '/mom-orders', array($controller, 'getOrders'));
	}

	/**
	 * assign product resource routes
	 *
	 * @param Store $store
	 */
	protected function setProductsRoute(Store $store)
	{
		/** @var ProductsController $controller */
		$controller = ControllerManager::get('Products@' . $this->name);
		$controller->setAddon($this->addon);
		$controller->setStore($store);
		
		// $payload = array( 'store_id' => 15 );
		// $storeToken = JWT::encode($payload, Config::get('site::secret_key'), 'HS512');

		// print_r($storeToken);
		// exit();
		
		$this->post($this->customerPath . '/mom-products', array($controller, 'productsManager'));

		// $this->post($this->customerPath . '/process-products', array($controller, 'productsManager'));
		

		$this->post($this->basePath . '/mom-products-remote', array($controller, 'getMOMProducts'));

		$this->post($this->basePath . '/mom-products', array($controller, 'productsManager'));
		$this->put($this->basePath . '/mom-products-update', array($controller, 'updateProducts'));

		// $this->get($this->basePath . '/mom-products/{id:.+}', array($controller, 'getProduct'));
	}

	/**
	 * HTTP method not allowed
	 */
    protected function methodNotAllowed()
	{
        // 405 Method Not Allowed
        http_response_code(405);

		exit;
    }

	/**
	 * route not found
	 */
    protected function routeNotFound()
	{
        // 404 Not Found
        http_response_code(404);

		exit;
    }
}