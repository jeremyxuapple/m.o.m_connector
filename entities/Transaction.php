<?php
namespace MiniBC\addons\momconnector\entities;

use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\BaseEntity;
use MiniBC\core\entities\Store;
use MiniBC\core\EntityFactory;
use MiniBC\core\interfaces\JSONAPIFormatInterface;
use MiniBC\core\interfaces\StoreSpecificDataInterface;

/**
 * Class Transaction
 *
 * @package MiniBC\addons\momconnector\entities
 */
class Transaction extends BaseEntity implements StoreSpecificDataInterface, JSONAPIFormatInterface
{
	const JSON_API_TYPE = 'mom-transaction';

	/** @var int internal id for this transaction */
	public $id = 0;

	/** @var int Bigcommerce Order id */
	public $orderId = 0;

	/** @var string transaction type (payment, refund) */
	public $type = 'payment';

	/** @var string credit card type (visa, mastercard, etc.) */
	public $creditCardType;

	/** @var string last 4 digits of credit card */
	public $creditCardLast4;

	/** @var \DateTime */
	public $creditCardExpiry;

	/** @var string transaction id assigned by the payment gateway */
	public $transactionId = '';

	/** @var string transaction gateway name */
	public $gatewayName = '';

	/** @var float transaction amount */
	public $amount = 0.00;

	/** @var \DateTime transaction date */
	public $date;

	/** @var string error message */
	public $error = '';

	/** @var array products associated with this transaction */
	public $products = array();

	/** @var Store store this transaction belongs to */
	public $store;

	/**
	 * Internal Variables
	 */
	protected $table = 'mom_transactions';
	protected $customer_id = 0;

	protected $creditCardExpiryDate = '';
	protected $expiryDateFormat = 'Y-m';

	protected $transactionDate = '';
	protected $transactionDateFormat = 'Y-m-d H:i:s';

	protected $schema = array(
		'id'					=> 'id',
		'customer_id'			=> 'customer_id',
		'orderId'				=> 'order_id',
		'type'					=> 'type',
		'transactionId'			=> 'transaction_id',
		'gatewayName'			=> 'gateway',
		'amount'				=> 'amount',
		'creditCardType'		=> 'cc_type',
		'creditCardLast4'		=> 'cc_last4',
		'creditCardExpiryDate'	=> 'cc_expiry',
		'transactionDate'		=> 'date',
		'error'					=> 'error'
	);

	/**
	 * constructor
	 *
	 * @param array $data transaction data
	 */
	public function __construct($data = array())
	{
		$this->on('load', function($event, $data) {
			$this->formatDataFromDb();
			$this->loadProducts();
		});

		$this->on('before.save', function($event) {
			$this->prepareDataForDb();
		});

		$this->on('after.save', function($event) {
			$this->saveProducts();
		});

		$this->on('before.update', function($event) {
			$this->prepareDataForDb();
		});

		$this->on('after.update', function($event) {
			$this->saveProducts();
		});

		if (!empty($data)) $this->populate($data);
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getJSONAPIType()
	{
		return self::JSON_API_TYPE;
	}

	/**
	 * {@inheritdoc}
	 */
	public function formatForJSONAPI($isInclude = false)
	{
		$data = array(
			'type'			=> self::JSON_API_TYPE,
			'id'			=> $this->id,
			'attributes' 	=> array(
				'transaction-id'	=> $this->transactionId,
				'amount'			=> $this->amount,
				'type'				=> $this->type,
				'cc-type'			=> $this->creditCardType,
				'cc-last4'			=> $this->creditCardLast4,
				'cc-expm'			=> $this->creditCardExpiry->format('m'),
				'cc-expy'			=> $this->creditCardExpiry->format('y'),
				'date'				=> $this->date->format('c'),
				'items'				=> $this->products,
				'error'				=> $this->error
			),
			'relationships' => array(
				'order'	=> array( 'type' => Order::getJSONAPIType(), 'id' => (int)$this->orderId )
			)
		);

		$response = array( 'data' => array( $data ), 'included' => array() );

		if ($isInclude) {
			$response['data'] = array();
			$response['included'] = array( $data );

			return $response;
		}

		// load order
		/** @var Order $order */
		$order = EntityFactory::makeFromId('Order', $this->orderId, 'momconnector');
		$orderData = $order->formatForJSONAPI(true);

		$response['included'] = array_merge($response['included'], $orderData['included']);

		return $response;
	}

	/**
	 * load products associated with this transaction
	 */
	protected function loadProducts()
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$products = $db->select(
			'mom_transaction_items',
			array(
				'customer_id'		=> $this->store->id,
				'transaction_id'	=> $this->id
			)
		);

		if (empty($products)) {
			$this->products = array();
		} else {
			$this->products = array_map(function($product) {
				return array(
					'id'	=> $product['transaction_id'],
					'name'	=> $product['mom_item_name'],
					'qty'	=> $product['qty'],
					'total'	=> $product['total']
				);
			}, $products);
		}
	}

	/**
	 * save transaction items to database
	 */
	protected function saveProducts()
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');

		// delete existing transaction items
		$db->delete(
			'mom_transaction_items',
			array(
				'transaction_id' => $this->id,
				'customer_id' => $this->store->id
			)
		);

		foreach ($this->products as $product) {
			$db->insert(
				'mom_transaction_items',
				array(
					'customer_id'		=> $this->store->id,
					'transaction_id'	=> $this->id,
					'order_id'			=> $this->orderId,
					'mom_item_id'		=> $product['id'],
					'mom_item_name'		=> $product['name'],
					'qty'				=> $product['qty'],
					'total'				=> $product['total']
				)
			);
		}
	}

	/**
	 * assign the store for this transaction
	 *
	 * @param Store $store
	 */
	public function setStore(Store $store)
	{
		$this->store = $store;
		$this->customer_id = $store->id;
	}

	/**
	 * format the data retrieved from db
	 */
	protected function formatDataFromDb()
	{
		$this->amount = (float)$this->amount;
		$this->customer_id = (int)$this->customer_id;

		if (!empty($this->creditCardExpiryDate)) {
			$this->creditCardExpiry = \DateTime::createFromFormat(
				$this->expiryDateFormat,
				$this->creditCardExpiryDate
			);
		}

		if (!empty($this->transactionDate)) {
			$this->date = \DateTime::createFromFormat(
				$this->transactionDateFormat,
				$this->transactionDate
			);
		}
	}

	/**
	 * prepare data to be inserted back into db
	 */
	protected function prepareDataForDb()
	{
		$this->customer_id = $this->store->id;

		if ($this->creditCardExpiry instanceof \DateTime) {
			$this->creditCardExpiryDate = $this->creditCardExpiry->format($this->expiryDateFormat);
		}

		if ($this->date instanceof \DateTime) {
			$this->transactionDate = $this->date->format($this->transactionDateFormat);
		}
	}
}