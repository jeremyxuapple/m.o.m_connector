<?php
namespace MiniBC\addons\momconnector\entities;

use MiniBC\addons\recurring\entities\CreditCardProfile;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\BaseEntity;
use MiniBC\core\entities\Store;
use MiniBC\core\EntityFactory;
use MiniBC\core\interfaces\JSONAPIFormatInterface;
use MiniBC\core\interfaces\StoreSpecificDataInterface;

/**
 * Class Order
 *
 * @package MiniBC\addons\momconnector\entities
 */
class Order extends BaseEntity implements StoreSpecificDataInterface, JSONAPIFormatInterface
{
	const JSON_API_TYPE = 'mom-order';

	/** @var int Bigcommerce Order Id */
	public $id = 0;

	/** @var string M.O.M. Order Id */
	public $momOrderId = 0;

	/** @var int minFraud risk score */
	public $riskScore = 100;

	/** @var float order total */
	public $total = 0.00;

	/** @var float SV status amount */
	public $svAmount = 0.00;

	/** @var bool true if the SV amount has been paid */
	public $svPaid = false;

	/** @var float amount outstanding amount on the order that needs to be paid */
	public $outstandingAmount = 0.00;

	/** @var string customer first name */
	public $customerFirstName = '';

	/** @var string customer last name */
	public $customerLastName = '';

	/** @var string customer email */
	public $customerEmail = '';

	/** @var string last 4 digits of credit card on file */
	public $creditCardLast4 = '';

	/** @var Store $store */
	public $store;

	/** @var Transaction[] transactions related to this order */
	public $transactions = array();

	public $onHold = false;

	/**
	 * Internal Variables
	 */
	protected $table = 'mom_orders';
	protected $customer_id = 0;
	protected $create_time = 0;
	protected $update_time = 0;

	protected $schema = array(
		'id'				=> 'bc_order_id',
		'momOrderId'		=> 'mom_order_id',
		'total'				=> 'order_total',
		'svAmount'			=> 'sv_amount',
		'svPaid'			=> 'sv_paid',
		'customer_id'		=> 'customer_id',
		'customerFirstName'	=> 'first_name',
		'customerLastName'	=> 'last_name',
		'customerEmail' 	=> 'email',
		'creditCardLast4' 	=> 'cc_last4',
		'riskScore'			=> 'risk_score',
		'onHold'			=> 'on_hold',
		'create_time'		=> 'create_time',
		'update_time'		=> 'update_time'
	);

	/**
	 * constructor
	 *
	 * @param array $data
	 */
	public function __construct($data = array())
	{
		$this->on('load', function($event, $data) {
			$this->formatDataFromDb();
			$this->loadTransactions();
			$this->calculateOutstandingAmount();
		});

		$this->on('before.save', function($event) {
			$this->prepareDataForDb();
		});

		$this->on('before.update', function($event) {
			$this->prepareDataForDb();
		});

		if (!empty($data)) $this->populate($data);
	}

	/**
	 * load transactions related to this order
	 *
	 * @throws \MiniBC\core\connection\exception\ConnectionException
	 * @throws \MiniBC\core\connection\exception\UnknownConnectionTypeException
	 * @throws \MiniBC\core\entities\exception\UnknownEntityException
	 */
	protected function loadTransactions()
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$transactions = $db->select(
			'mom_transactions',
			array(
				'order_id' => $this->id,
				'customer_id' => $this->store->id
			)
		);

		if (!empty($transactions)) {
			$this->transactions = array();

			foreach ($transactions as $transactionData) {
				/** @var Transaction $transaction */
				$transaction = EntityFactory::makeFromId('Transaction', $transactionData['id'], 'momconnector');
				$transaction->setStore($this->store);

				$this->transactions[] = $transaction;
			}
		}
	}

	/**
	 * calculate the outstanding amount for this order
	 */
	protected function calculateOutstandingAmount()
	{
		$outstanding = (float)$this->total;

		if ($this->svAmount > $outstanding) {
			$outstanding = $this->svAmount;
		}

		foreach ($this->transactions as $transaction) {
			// transaction resulted in an error
			if (!empty($transaction->error)) continue;

			if ($transaction->type == 'payment' || $transaction->type == 'refunded') {
				// payment against order total
				$outstanding = $outstanding - (float)$transaction->amount;
				continue;
			}

			// refund a payment previously made for this order
			$outstanding = $outstanding + (float)$transaction->amount;
		}

		if ($outstanding > $this->total && $this->svAmount <= 0) {
			// outstanding amount cannot be greater than the order total
			$outstanding = (float)$this->total;
		}

		if ($outstanding <= 0) {
			// outstanding amount cannot be less than 0
			$outstanding = 0;
		}

		$this->outstandingAmount = $outstanding;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getJSONAPIType()
	{
		return self::JSON_API_TYPE;
	}

	/**
	 * {@inheritdoc}
	 */
	public function formatForJSONAPI($isInclude = false)
	{
		// load payment profile
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$profile = $db->selectFirst(
			'mom_payment_profiles',
			array( 'customer_id' => $this->store->id, 'order_id' => $this->id )
		);

		if (empty($profile)) {
			return array( 'data' => array(), 'included' => array() );
		}

		/** @var CreditCardProfile $creditCard */
		$creditCard = EntityFactory::madeFromFields(
			'CreditCardProfile',
			array(
				'gatewayId'	=> $profile['profile_id'],
				'customer_id' => $this->store->id
			),
			'recurring'
		);

		$data = array(
			'type'			=> self::JSON_API_TYPE,
			'id'			=> $this->id,
			'attributes' 	=> array(
				'mom-order-id'	=> $this->momOrderId,
				'total'			=> $this->total,
				'first-name'	=> $this->customerFirstName,
				'last-name'		=> $this->customerLastName,
				'email'			=> $this->customerEmail,
				'outstanding'	=> $this->outstandingAmount,
				'cc-type'		=> $creditCard->cardType,
				'cc-last4'		=> $creditCard->lastFourDigits,
				'risk-score'	=> $this->riskScore,
				'profile-id'	=> $creditCard->id,
				'on-hold'		=> $this->onHold
			)
		);

		if ($creditCard->expiryDate) {
			$data['attributes']['cc-expm'] = $creditCard->expiryDate->format('m');
			$data['attributes']['cc-expy'] = $creditCard->expiryDate->format('y');
		}

		if ($creditCard->customerProfile) {
			$data['attributes']['user-id'] = $creditCard->customerProfile->userId;
		}

		$include = array();

		if (!empty($this->transactions)) {
			$transactions = array();
			$transactionType = Transaction::getJSONAPIType();

			foreach (array_reverse($this->transactions) as $transaction) {
				/** @var Transaction $transaction */
				$transactions[] = array( 'type'	=> $transactionType, 'id' => $transaction->id );
				$transactionData = $transaction->formatForJSONAPI(true);

				$include = array_merge($include, $transactionData['included']);
			}

			$data['relationships'] = array(
				'transactions' => array( 'data' => $transactions )
			);
		}

		$response = array( 'data' => array( $data ), 'included' => $include );

		if ($isInclude) {
			$response['data'] = array();
			$response['included'][] = $data;
		}

		return $response;
	}

	/**
	 * assign the store for this order
	 *
	 * @param Store $store
	 */
	public function setStore(Store $store)
	{
		$this->store = $store;
		$this->customer_id = $store->id;
	}

	/**
	 * format the data retrieved from db
	 */
	protected function formatDataFromDb()
	{
		$this->customer_id = (int)$this->customer_id;
		$this->total = (float)$this->total;
		$this->onHold = (bool)$this->onHold;
		$this->svAmount = (float)$this->svAmount;
		$this->svPaid = ((int)$this->svPaid > 0);
	}

	/**
	 * prepare data to be inserted back into db
	 */
	protected function prepareDataForDb()
	{
		$this->customer_id = $this->store->id;
	}
}