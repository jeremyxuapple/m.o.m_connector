<?php
namespace MiniBC\addons\momconnector\services;

use GuzzleHttp\Client;

use MiniBC\addons\momconnector\entities\Order;
use MiniBC\bigcommerce\services\StorefrontAssetsService;

use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\EntityFactory;
use MiniBC\core\entities\Customer;
use MiniBC\core\entities\Store;
use MiniBC\core\interfaces\SingletonInterface;

/**
 * Class MOMService
 *
 * operations for M.O.M.
 *
 * @package MiniBC\addons\momconnector\services
 */
class MOMService implements SingletonInterface
{
	protected static $instance;

	/**
	 * sends API request to the M.O.M. server
	 *
	 * @param array	$data 	data to be sent to the API
	 * @param Store $store	store instance
	 * @return \stdClass	response object
	 * @throws \MiniBC\core\entities\exception\UnknownEntityException
	 * @throws \MiniBC\core\entities\exception\UnsupportedPopulationMethodException
	 */
	public function sendAPIRequest($data, Store $store)
	{
		$response = new \stdClass;
		$response->message = '';
		$response->success = false;

		try {
			/** @var MySQLConnection $db */
			$db = ConnectionManager::getInstance('mysql');
			$settings = $db->selectFirst(
				'mom_settings',
				array ('customer_id' => $store->id)
			);

			if (empty($settings)) {
				$response->message = 'Failed to retrieve server settings. Please try again later.';
				return $response;
			}
		} catch (\Exception $e) {
			$response->message = 'Internal error. Please try again later.';
			return $response;
		}

		/** @var Customer $customer */
		$customer = EntityFactory::makeFromId('Customer', $store->id);

		$assetService = new StorefrontAssetsService($customer);
		$storeHash = $assetService->getStoreHash();

		$headers = array( 'X-MBC-AUTH' => $storeHash );

		// add basic auth header if required
		if (!empty($settings['username']) && !empty($settings['password'])) {
			$headers['Authorization'] = 'Basic ' . base64_encode( $settings['username'] . ':' . $settings['password'] );
		}

		try {
			$client = new Client();
	
			// send request to endpoint on MOM machine
			$apiResponse = $client->post($settings['api_endpoint'], array(
				'json' 		=> $data,
				'headers'	=> $headers
			));

		} catch (\Exception $e) {

			var_dump($e);


			$response->message = 'An error occurred when attempting to contact the M.O.M. server. Please try again later.';
			return $response;
		}

		$body = $apiResponse->getBody()->getContents();
	
		$responseStatus = $apiResponse->getStatusCode();

		if ($responseStatus != 200) {
			// connection error
			$response->message = 'Failed to connect to M.O.M. server. Please ensure that the web server is running.';
			return $response;
		}

		$response->message = $body;
		$response->success = true;

		return $response;
	}

	/**
	*	Get MOM product information
	*
	*/
	public function getMOMProducts(Store $store)
	{
		$response = new \stdClass;
		$response->message = 'action failed';
		$response->success = false;
		$response->products = array();

		$data = array( 'action' => 'get_products');

		$apiResponse = $this->sendAPIRequest($data, $store);
		
		if ($apiResponse->success) {
			return $apiResponse->message;
		}

	}

	/**
	 * get new orders from M.O.M.
	 *
	 * @param Store $store
	 * @return \stdClass
	 */
	public function getMOMOrders(Store $store)
	{
		$response = new \stdClass;
		$response->message = '';
		$response->success = false;
		$response->orders = array();

		$data = array( 'action' => 'get_orders' );

		$apiResponse = $this->sendAPIRequest($data, $store);

		if (!$apiResponse->success) {
			$response->message = $apiResponse->message;
			return $response;
		}

		$data = json_decode($apiResponse->message, true);

		if (!is_array($data) || empty($data['orders'])) {
			// no matching orders in MOM
			$response->message = 'No matching orders were found in M.O.M., please download new orders from BizSync and try again.';
			return $response;
		}

		foreach ($data['orders'] as $orderData) {
			if (!$this->insertOrder($orderData['bc_order_id'], $orderData['mom_order_id'], (bool)$orderData['hold'], $orderData['order_total'], $orderData['sv_amount'], $store)) {
				continue;
			}

			$total = (float)$orderData['total'];
			$svAmount = (float)$orderData['sv_amount'];

			if ($svAmount > 0) {
				if (!$this->isSVAmountPaid($orderData['bc_order_id'], $store->id)) {
					$total += $svAmount;
				}
			}

			if ($total <= 0) {
				continue;
			}

			$response->orders[] = array( 'id' => $orderData['bc_order_id'], 'amount' => $total );
		}

		$response->success = true;
		return $response;
	}

	protected function isSVAmountPaid($orderId, $storeId)
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$momOrder = $db->selectFirst('mom_orders', array( 'bc_order_id' => $orderId, 'customer_id'=> $storeId ));

		if (empty($momOrder)) return false;

		return ((int)$momOrder['sv_paid'] > 0);
	}

	/**
	 * get refunds from M.O.M.
	 *
	 * @param Store $store
	 * @return \stdClass
	 */
	public function getMOMRefunds(Store $store)
	{
		$response = new \stdClass;
		$response->message = '';
		$response->success = false;
		$response->refunds = array();

		$data = array( 'action' => 'get_refunds' );

		$apiResponse = $this->sendAPIRequest($data, $store);

		if (!$apiResponse->success) {
			$response->message = $apiResponse->message;
			return $response;
		}

		$data = json_decode($apiResponse->message, true);

		if (!is_array($data) || empty($data['orders'])) {
			$response->message = 'No refunds were found in M.O.M.';
			return $response;
		}

		foreach ($data['orders'] as $refundData) {
			// calculate total refund amount
			$total = $this->calculateRefundTotal($refundData['items'], $store);

			// nothing to refund
			if ($total <= 0) continue;

			$response->refunds[] = array(
				"id" => $refundData['bc_order_id'],
				"amount" => $total,
				"items"	=> $refundData['items']
			);
		}

		$response->success = true;

		return $response;
	}

	protected function calculateRefundTotal($items, Store $store)
	{
		if (empty($items)) return 0;

		$total = 0;

		foreach ($items as $item) {
			if ($this->returnedItemProcessed($item['id'], $store)) {
				continue;
			}

			$total += (float)$item['amount'];
		}

		return $total;
	}

	/**
	 * insert new order for M.O.M. in MiniBC
	 *
	 * @param int $orderId	Bigcommerce order id
	 * @param int $momId 	M.O.M. order id
	 * @param bool $onHold 	set to true if this order is on permanent hold
	 * @param float $orderTotal order total
	 * @param float	$svAmount 	total amount for item in SV status
	 * @param Store $store 	store this order belongs to
	 * @return bool
	 * @throws \MiniBC\core\connection\exception\ConnectionException
	 * @throws \MiniBC\core\connection\exception\UnknownConnectionTypeException
	 */
	public function insertOrder($orderId, $momId, $onHold, $orderTotal, $svAmount, Store $store)
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');

		// check if order already exists
		$where = array(
			'customer_id' 	=> $store->id,
			'bc_order_id' 	=> $orderId
		);

		$exists = $db->selectFirst( 'mom_orders', $where );

		if (!empty($exists)) {
			// order already exists, update on hold status
			$result = $db->update(
				'mom_orders',
				array(
					'mom_order_id' 	=> $momId,
					'on_hold' 		=> $onHold,
					'order_total'	=> $orderTotal,
					'sv_amount'		=> $svAmount
				),
				$where
			);

			return ($result !== false);
		}

		// check if order exist in bigbackup
		$orderData = $db->selectFirst(
			'bigbackup_bc_orders',
			array(
				'customer_id'	=> $store->id,
				'bc_id'			=> $orderId
			)
		);

		// order not found in bigbackup
		if (empty($orderData)) return false;

		$now = time();

		$insertData = array(
			'bc_order_id'	=> $orderId,
			'customer_id'	=> $store->id,
			'mom_order_id'	=> $momId,
			'order_total'	=> $orderData['total_inc_tax'],
			'sv_amount'		=> $svAmount,
			'first_name'	=> $orderData['billing_first_name'],
			'last_name'		=> $orderData['billing_last_name'],
			'email'			=> $orderData['billing_email'],
			'cc_last4'		=> '',
			'on_hold'		=> $onHold,
			'create_time'	=> $now,
			'update_time'	=> $now
		);

		// get payment profile associated with this order
		$profileResult = $db->queryFirst(
			'
			SELECT profile.*
			FROM `rc_payment_profiles` profile
			LEFT JOIN `mom_payment_profiles` rel
			ON rel.customer_id = profile.customer_id 
				AND rel.profile_id = profile.profile_id
			WHERE profile.customer_id = :customer_id 
				AND rel.order_id = :order_id 
				AND profile.method = "credit_card"
			',
			array(
				':customer_id' 	=> $store->id,
				':order_id'		=> $orderId
			)
		);

		// payment profile not found
		if (empty($profileResult)) return false;

		if (!empty($profileResult['gateway_data'])) {
			$data = unserialize($profileResult['gateway_data']);

			if (!empty($data) && !empty($data['last4'])) {
				$insertData['cc_last4'] = $data['last4'];
			}
		}

		$result = $db->insert('mom_orders', $insertData);

		return ($result !== false);
	}

	/**
	 * check if an returned item has already been processed
	 *
	 * @param string $itemId
	 * @param Store $store
	 * @return bool returns true if the item has already been processed
	 */
	public function returnedItemProcessed($itemId, Store $store)
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$refund = $db->selectFirst(
			'mom_transaction_items',
			array( 'customer_id' => $store->id, 'mom_item_id' => $itemId )
		);

		return (!empty($refund));
	}

	/**
	 * push all transaction ids related to an order to M.O.M.
	 *
	 * @param int $orderId Bigcommerce Order ID
	 * @param Store $store
	 * @return \stdClass
	 */
	public function pushTransactionIds($orderId, Store $store)
	{
		$response = new \stdClass;
		$response->success = false;
		$response->message = '';

		/** @var Order $order */
		$order = EntityFactory::madeFromFields(
			'Order',
			array( 'customer_id' => $store->id, 'id' => $orderId ),
			'momconnector'
		);

		$transactionIds = array();
		$amountPaid = 0;

		foreach ($order->transactions as $transaction) {
			if (!empty($transaction->transactionId) && !in_array($transaction->transactionId, $transactionIds)) {
				$transactionIds[] = $transaction->transactionId;
			}

			// transaction resulted in an error
			if (!empty($transaction->error)) continue;

			if ($transaction->type == 'payment' || $transaction->type == 'refunded') {
				$amountPaid += $transaction->amount;

				continue;
			}

			$amountPaid -= $transaction->amount;
		}

		$transactionStr = implode(',', $transactionIds);

		$data = array(
			'action' => 'push_transaction',
			'transaction' => array(
				'bc_order_id'	=> $order->id,
				'mom_order_id'	=> $order->momOrderId,
				'amount'		=> $amountPaid,
				'transaction_id'=> $transactionStr
			)
		);

		$apiResponse = $this->sendAPIRequest($data, $store);

		if (!$apiResponse->success) {
			$response->message = $apiResponse->message;
			return $response;
		}

		$response->success = true;
		return $response;
	}

	/**
	 * returns a list of orders that needs to be updated from M.O.M.
	 *
	 * @param Store $store
	 * @return array list of Bigcommerce order ids
	 */
	public function getLatestOrders(Store $store)
	{
		try {
			/** @var MySQLConnection $db */
			$db = ConnectionManager::getInstance('mysql');

			$query = '
			SELECT orders.bc_order_id 
			FROM mom_orders orders
			LEFT JOIN (
				SELECT order_id, customer_id, SUM(amount) AS total 
				FROM mom_transactions 
				WHERE customer_id = :customer_id 
				AND (type = "payment" OR type = "refunded")
				GROUP BY order_id
			) AS trans
			ON trans.order_id = orders.bc_order_id AND trans.customer_id = orders.customer_id
			WHERE orders.customer_id = :customer_id
			AND (
				mom_order_id < 1 
				OR on_hold > 0
				OR trans.total < orders.order_total
			)
			';

			$results = $db->query($query, array( ':customer_id' => $store->id ));

			if (empty($results)) return array();

			return array_map(function($row) {
				return (int)$row['bc_order_id'];
			}, $results);
		} catch (\Exception $e) {
			return array();
		}
	}


	/**
	 * returns an instance of this class
	 *
	 * @return MOMService
	 */
	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}