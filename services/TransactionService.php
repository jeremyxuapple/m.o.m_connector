<?php
namespace MiniBC\addons\momconnector\services;

use MiniBC\addons\momconnector\entities\Transaction;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Store;
use MiniBC\core\EntityFactory;
use MiniBC\core\interfaces\SingletonInterface;

class TransactionService implements SingletonInterface
{
	/** @var TransactionService|null $instance current active instance */
	protected static $instance = null;

	const DB_DATE_FORMAT = 'Y-m-d H:i:s';
	const RESULTS_PER_PAGE = 10;

	/**
	 * get a list of transactions
	 *
	 * @param int       $page
	 * @param string    $type
	 * @param \DateTime $startDate
	 * @param \DateTime $endDate
	 * @param Store     $store
	 * @return Transaction[]
	 */
	public function getTransactions($page = 1, $type = 'all', \DateTime $startDate, \DateTime $endDate, Store $store)
	{
		$transactions = array();

		// build query
		$query = 'SELECT * FROM mom_transactions ' . $this->buildQueryWhere($type);

		if (is_numeric($page)) {
			// calculate offset
			$offset = ($page - 1) * self::RESULTS_PER_PAGE;

			// limit
			$query .= sprintf(' LIMIT %d, %d', $offset, self::RESULTS_PER_PAGE);
		}

		// query params
		$where = array(
			'customer_id' 	=> $store->id,
			'start_date'	=> $startDate->format(self::DB_DATE_FORMAT),
			'end_date'		=> $endDate->format(self::DB_DATE_FORMAT)
		);

		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$results = $db->query($query, $where);

		if (empty($results)) return array();

		foreach ($results as $row) {
			$transaction = EntityFactory::makeFromId('Transaction', $row['id'], 'momconnector');
			$transactions[] = $transaction;
		}

		return $transactions;
	}

	/**
	 * get the total number of transactions
	 *
	 * @param string    $type
	 * @param \DateTime $startDate
	 * @param \DateTime $endDate
	 * @param Store     $store
	 * @return int		number of transactions
	 */
	public function getTransactionCount($type, \DateTime $startDate, \DateTime $endDate, Store $store)
	{
		// build query
		$query = 'SELECT COUNT(id) AS total FROM mom_transactions ' . $this->buildQueryWhere($type);

		// query params
		$where = array(
			'customer_id' 	=> $store->id,
			'start_date'	=> $startDate->format(self::DB_DATE_FORMAT),
			'end_date'		=> $endDate->format(self::DB_DATE_FORMAT)
		);

		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');
		$result = $db->queryFirst($query, $where);

		if (empty($result)) return 0;

		return (int)$result['total'];
	}

	/**
	 * build where clause for database query
	 *
	 * @param string $type
	 * @return string
	 */
	protected function buildQueryWhere($type)
	{
		$query = '
		WHERE customer_id = :customer_id
		AND date > :start_date
		AND date <= :end_date
		';

		if ($type == 'error') $query .= ' AND error != ""';
		else if ($type == 'refund') $query .= ' AND (type = "refund" OR type = "return")';
		else if ($type == 'payment') $query .= ' AND (type = "payment" OR type = "refunded")';

		return $query;
	}

	/**
	 * get an instance of this class
	 *
	 * @return TransactionService
	 */
	public static function getInstance()
	{
		if (!is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}