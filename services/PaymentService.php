<?php
namespace MiniBC\addons\momconnector\services;

use MiniBC\addons\momconnector\entities\Order;
use MiniBC\addons\momconnector\entities\Transaction;

use MiniBC\addons\recurring\entities\CreditCardProfile;
use MiniBC\addons\recurring\gateways\AuthorizeNet;
use MiniBC\addons\recurring\services\PaymentProfileService;
use MiniBC\addons\recurring\services\PaymentService as RecurringPayment;

use MiniBC\core\EntityFactory;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Store;
use MiniBC\core\interfaces\SingletonInterface;

/**
 * Class PaymentService
 *
 * contains payment related functionality
 *
 * @package MiniBC\addons\momconnector\services
 */
class PaymentService implements SingletonInterface
{
	protected static $instance;

	/**
	 * refund payment for an order
	 *
	 * @param int $orderId		Bigcommerce order ID
	 * @param float $amount		refund amount
	 * @param string $type		transaction type (return, refund)
	 * @param array $items		transaction items
	 * @param Store $store		store instance
	 * @return array			payment result
	 */
	public function refundPayment($orderId, $amount, $type, $items = array(), Store $store)
	{
		$paymentResult = array(
			'http_code'	=> 400,
			'success' 	=> false,
			'message'	=> ''
		);

		try {
			$result = $this->getPaymentInfo($orderId, $store);

			if (empty($result)) {
				// failed to find associating payment profile
				$paymentResult['message'] = 'Failed to find payment method associated with this order.';
				return $paymentResult;
			}

			/** @var Order $orders */
			$orders = EntityFactory::madeFromFields(
				'Order',
				array( 'customer_id' => $store->id, 'id' => $result['bc_order_id'] ),
				'momconnector'
			);

			// check paid amount
			$maxAmount = $orders->total - $orders->outstandingAmount;

			if ($amount > $maxAmount) {
				$paymentResult['message'] = 'Refund amount cannot be greater than the amount paid.';

				return $paymentResult;
			}

			$paymentService = RecurringPayment::getInstance();
			$profileService = PaymentProfileService::getInstance();

			/** @var AuthorizeNet $gateway */
			$gateway = $paymentService->getGateway($result['gateway'], $store);

			/** @var CreditCardProfile $profile */
			$profile = $profileService->getFromGatewayId($result['profile_id'], $gateway->gatewayName, $store);

			$response = $gateway->refundTokenizedPayment($amount, '', $profile);
			$paymentStatus = $response->getStatus();

			/** @var Transaction $refundTransaction refund transaction */
			$refundTransaction = EntityFactory::make('Transaction', array(), 'momconnector');
			$refundTransaction->amount = $amount;
			$refundTransaction->creditCardType = $profile->cardType;
			$refundTransaction->creditCardExpiry = $profile->expiryDate;
			$refundTransaction->creditCardLast4 = $profile->lastFourDigits;
			$refundTransaction->orderId = $result['bc_order_id'];
			$refundTransaction->transactionId = $response->getTransactionId();
			$refundTransaction->gatewayName = $result['gateway'];
			$refundTransaction->products = $items;
			$refundTransaction->type = $type;
			$refundTransaction->date = new \DateTime();
			$refundTransaction->setStore($store);

			if ($paymentStatus != 'payment_success') {
				$refundTransaction->error = $response->getMessage();
			}

			$refundTransaction->save();

			if (!empty($refundTransaction->error)) {
				// show error
				$paymentResult['message'] = $response->getMessage();
				return $paymentResult;
			}

			// push transaction id to MOM
			MOMService::getInstance()->pushTransactionIds($result['bc_order_id'], $store);

			$paymentResult['http_code'] = 200;
			$paymentResult['success'] = true;

			return $paymentResult;
		} catch (\Exception $e) {
			$paymentResult['http_code'] = 500;
			$paymentResult['message'] = 'Internal error, please try again at a later time.';

			return $paymentResult;
		}
	}

	/**
	 * create a new payment for an order
	 *
	 * @param int 	$orderId 	Bigcommerce Order ID
	 * @param float $amount		payment amount
	 * @param Store $store		store instance
	 * @return array			payment result
	 */
	public function createPayment($orderId, $amount, Store $store)
	{
		$paymentResult = array(
			'http_code'	=> 400,
			'success' 	=> false,
			'message'	=> ''
		);

		try {
			$result = $this->getPaymentInfo($orderId, $store);

			if (empty($result)) {
				// failed to find associating payment profile
				$paymentResult['message'] = 'Failed to find payment method associated with this order.';
				return $paymentResult;
			}

			/** @var Order $orders */
			$orders = EntityFactory::madeFromFields(
				'Order',
				array( 'customer_id' => $store->id, 'id' => $result['bc_order_id'] ),
				'momconnector'
			);

			// check outstanding amount
			$maxAmount = $orders->outstandingAmount;

			if ($amount > $maxAmount) {
				$paymentResult['message'] = 'Payment amount cannot be greater than the outstanding amount.';

				return $paymentResult;
			}

			$paymentService = RecurringPayment::getInstance();
			$profileService = PaymentProfileService::getInstance();

			/** @var AuthorizeNet $gateway */
			$gateway = $paymentService->getGateway($result['gateway'], $store);

			/**
			 * check transaction details to see if authorization is still valid
			 */
			$status = $gateway->getTransactionStatus($result['transaction_id']);

			/** @var CreditCardProfile $profile */
			$profile = $profileService->getFromGatewayId($result['profile_id'], $gateway->gatewayName, $store);

			if ($status == 'authorizedPendingCapture') {
				$response = $gateway->captureAuthorizedPayment($amount, $result['transaction_id']);
			} else {
				// authorization expired, auth and capture
				$gateway->setPaymentTransactionType('authCaptureTransaction');
				$response = $gateway->createTokenizedPayment(
					$amount,
					$profile,
					array(
						'orderId' => $orderId,
						'description' => 'Payment for Order No. ' . $orderId
					)
				);
			}

			$paymentStatus = $response->getStatus();

			/** @var Transaction $transaction */
			$transaction = EntityFactory::make('Transaction', array(), 'momconnector');
			$transaction->amount = $amount;
			$transaction->creditCardType = $profile->cardType;
			$transaction->creditCardExpiry = $profile->expiryDate;
			$transaction->creditCardLast4 = $profile->lastFourDigits;
			$transaction->orderId = $result['bc_order_id'];
			$transaction->transactionId = $response->getTransactionId();
			$transaction->gatewayName = $result['gateway'];
			$transaction->type = 'payment';
			$transaction->date = new \DateTime();
			$transaction->setStore($store);

			if ($paymentStatus != 'payment_success') {
				$transaction->error = $response->getMessage();
			}

			$transaction->save();

			if (!empty($transaction->error)) {
				// show error
				$paymentResult['message'] = $response->getMessage();
				return $paymentResult;
			}

			// push transaction id to MOM
			MOMService::getInstance()->pushTransactionIds($result['bc_order_id'], $store);

			$paymentResult['http_code'] = 200;
			$paymentResult['success'] = true;

			return $paymentResult;
		} catch (\Exception $e) {
			$paymentResult['http_code'] = 500;
			$paymentResult['message'] = 'Internal error, please try again at a later time.';

			return $paymentResult;
		}
	}

	/**
	 * retrieve the payment information for an order
	 *
	 * @param int $orderId	order ID
	 * @param Store $store	store instance
	 * @return bool|array 	returns false on failure
	 */
	protected function getPaymentInfo($orderId, Store $store)
	{
		/** @var MySQLConnection $db */
		$db = ConnectionManager::getInstance('mysql');

		$result = $db->queryFirst(
			'
				SELECT orders.*, profiles.*
				FROM mom_orders orders
				INNER JOIN mom_payment_profiles profiles
				ON ( profiles.order_id = orders.bc_order_id AND profiles.customer_id = orders.customer_id )
				WHERE orders.customer_id = :customer_id AND orders.bc_order_id = :order_id
				LIMIT 0, 1
				',
			array(
				':customer_id'	=> $store->id,
				':order_id'		=> $orderId
			)
		);

		if (empty($result)) return false;

		return $result;
	}

	/**
	 * returns an instance of this class
	 *
	 * @return PaymentService
	 */
	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}
