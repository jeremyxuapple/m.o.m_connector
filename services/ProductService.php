<?php
namespace MiniBC\addons\momconnector\services;

use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\connection\MySQLConnection;
use MiniBC\core\entities\Store;
use MiniBC\core\EntityFactory;
use MiniBC\core\interfaces\SingletonInterface;
use MiniBC\core\Auth;


class ProductService implements SingletonInterface
{
	protected static $instance;

	public function __construct() 
	{
		$this->customer = Auth::getInstance()->getCustomer();
		$this->db = ConnectionManager::getInstance('mysql');
	}

	/**
	* Format data from M.O.M server for product creation and update
	*
	* @param array	$data data to be sent to the API
	*
	* @return mixed: the formatted product contains product basic info, bulk pricing, custom fields, images
	*/

	public function dataFormatter($data)
	{		
		$momProduct = $data;

		$product = array(
			// 'name' => 'Product' . rand(10, 10000),
			'name' => $momProduct['inetsdesc'],
			'sku' => $momProduct['number'],
			'type' => 'physical',
			'is_visible' => true,
			'page_title' => $momProduct['inetsdesc'],
			'description' => $momProduct['inetfdesc'],
			'search_keywords' => $momProduct['inetkeywrd'],
			'price' => number_format($momProduct['price1'], 4, '.', ''),
			'categories' => $this->categorySplit($momProduct['assoc']),
			'weight' => number_format($momProduct['unitweight'], 4, '.', ''),
			'width' => number_format($momProduct['bwidth'], 4, '.', ''),
			'height' => number_format($momProduct['bheight'], 4, '.', ''),
			'depth' => number_format($momProduct['blength'], 4, '.', ''),
			'inventory_level' => intval($momProduct['units']),
		);

		if (!empty($momProduct['upccode'])) {
			$product['upc'] = (string)$momProduct['upccode'];
		}

		$inventory_tracking = ($momProduct['construct'] && $momProduct['dropship']) ? 'simple' : 'none';
		$product['inventory_tracking'] = $inventory_tracking;

		$product["availability"] = $momProduct['discont'] ? 'available' : 'disabled';

			// Format expected date to Big Commerce RFC2822 time format
		if ( $momProduct['EXPECT_ON'] != null ) {

			$product["availability"] = 'preorder';
			$expected_date = date_create_from_format('Y-m-d H:i:s.u', $momProduct['EXPECT_ON'])->getTimestamp();
			$releaseDate = date('r', $expected_date);

			$product['preorder_release_date'] = $releaseDate;
			$product['preorder_message'] = "This item is a pre-order or special-order and will be available on $releaseDate";
		}	
		
		$formattedProduct['product'] = $product;
		
		/******* Product Images Section **********/ 
	
		// $images = array_filter($momProduct, function ($value) {
		// 	if ( is_string($value) ) {
		// 		if ( 
		// 			strpos($value, '.JPG') 
		// 			|| strpos($value, '.jpg')
		// 			|| strpos($value, '.GIF')
		// 			|| strpos($value, '.gif') 
		// 			|| strpos($value, '.PNG') 
		// 			|| strpos($value, '.png') 
		// 			|| strpos($value, '.JPEG')
		// 			|| strpos($value, '.jpeg')  
		// 			|| strpos($value, '.JFIF')
		// 			|| strpos($value, '.jfif')
		// 		) return true;
		// 	}
		// });

		// $formattedProduct['images'] = $images;

		// Grabbing WEBDAV_URL for images

		$formattedProduct['images'] = $momProduct['WEBDAV_URL'];

		/******* Bulk Pricing Section **********/ 

		if ($momProduct['CTYPE'] == 'W') {
			
			$bulkPricing = array(
				'type' => 'fixed',
				// 'min' => $momProduct['qty'],
				'type_value' => $momProduct['WHOLESALE_PRICE']
			);

			$formattedProduct['bulkPricing'] = $bulkPricing;
		}

		/******* Custom Fields Section **********/ 

		$discountable = $momProduct['nodsct'] ? 'NO' : 'YES';
		$discontinued = $momProduct['discont'] ? 'YES' : 'NO';

		$customFields = array(
			// '1 CATCH ALL' => $momProduct['desc2'],
			'ISBN' => $momProduct['isbn'],
			// 'UPC' => $momProduct['upccode'],
			// 'ASSOC' => $momProduct['assoc'],
			'Genre' => $momProduct['advanced1'],
			'If Can Be Discounted' => $discountable,
			'Discontinued' => $discontinued,
		);

		$formattedProduct['customFields'] = $customFields;
		
		return $formattedProduct;
	}

	/**
	* Filter the category base on the fiedl of assoc
	*
	* @param - $assoc: String
	*
	* @return - $catIds: Array(containing the ids this particular product belogs to)
	*/

	public function categorySplit($assoc)
	{	
		$catIds = [24];
		switch ($assoc) {
			case 'R':
				array_push($catIds, 31);
				break;
			case 'C':
				array_push($catIds, 32, 57);
				break;
			case 'X':
				array_push($catIds, 32, 58);
				break;
			case 'S':
				array_push($catIds, 32, 59);
				break;
			case 'B':
				array_push($catIds, 32, 60);
				break;
			case 'K2':
				array_push($catIds, 32, 61);
				break;
			case 'U':
				array_push($catIds, 32, 62);
				break;
			case 'DA':
				array_push($catIds, 32, 63);
				break;
			case 'DV':
				array_push($catIds, 32, 63);
				break;
			case 'T':
				array_push($catIds, 33);
				break;
			default:
				break;
		}

		return $catIds;
	}

	/**
	* Select the MOMProducts array base on the product skus
	*
	* @param - $skus: the skus array
	*
	* @param - $MOMProducts: the whole MOMProducts received 
	*
	* @return - $momProductsArray: the momProducts array ready for being processed
	*/

	public function selectMOMProducts($skus, $MOMProducts)
	{
			foreach ($skus as $sku) {
				foreach ($MOMProducts as $p) {
					if ( $sku == $p['number'] ) 
						{
							$selectedMOMProducts[] = $p;
							break;
						}
				}
			}

			return $selectedMOMProducts;
	}

	/**
	* Select the BCProducts array base on the product skus
	*
	* @param - $skus: the skus array
	*
	* @param - $MOMProducts: the whole MOMProducts received 
	*
	* @param - $BCProducts: the whole BCProducts received 
	*
	* @return - $updateProducts: the updateProducts array ready for being processed
	*/

	public function assembleUpdateProducts($skus, $MOMProducts, $BCProducts)
	{			
		$i = 0;
		foreach ($skus as $sku) {		
			foreach ($MOMProducts as $p) {
				if ( $sku == $p['number'] ) 
					{	
						$updateProducts[$i]['momProduct'] = $this->dataFormatter($p);
						break;
					}
				}

			foreach ($BCProducts as $p) {
				if ( $sku == $p['sku']) 
					{
						$updateProducts[$i]['bcProduct'] = $p;
						break;
					}
			}

			$i ++;
		}

		return $updateProducts;
	}

	/**
	* Process Basic Product Information
	*
	* @param - $bcProduct: Product array from BigCommerce
	*
	* @param - $momProduct: Product array from M.O.M
	*
	* @param - $api: API client for connecting with the store
	*/

	public function processBasicProductInfo($bcProduct, $momProduct, $api)
	{	

		foreach ($momProduct as $key => $value) {
			$bcProductBasicInfo[$key] = $bcProduct[$key];
		} 		
		// array_diff function does not take multiple dimension array for comparasion, so we will take out categories.
		$momProduct_c = $momProduct['categories'];
		$bcProductBasicInfo_c = $bcProductBasicInfo['categories'];

		unset($momProduct['categories']);
		unset($bcProductBasicInfo['categories']);

		$diff = array_diff($momProduct, $bcProductBasicInfo);

		if ( $momProduct_c != $bcProductBasicInfo_c ) $diff['categories'] = $momProduct_c;

		$api::updateProduct($bcProduct['bc_id'], $diff);
		print_r($api::getLastError());
	}


	/**
	* Process Images
	*
	* @param - $productId: Product Id collect from the product level, in case there is no images associated with the product yet
	*
	* @param - $momImages: Image array from M.O.M
	*
	* @param - $api: API client for connecting with the store
	*/

	public function processImages($productId, $momImages, $api)
	{	
		// The url will not be the same even the images are all the same as before, we will delete all the images and then create images from the data passed in
		$customerId = $this->customer->id;

		$bcImages = $this->db->query('
			SELECT bc_id, product_id, image_file
			FROM bigbackup_bc_products_images
			WHERE customer_id = :customer_id
			AND product_id = :product_id'
			,
			array(
				'customer_id' => $customerId,
				'product_id' => $productId
			)
		);

		if ( $bcImages != null ) {
			foreach ($bcImages as $image) {
				$api::deleteProductImage($productId, $image['bc_id']);
			}
		}
	
		// Create the images from the momImages
		foreach ($momImages as $key => $url) {
			$api::createProductImage($productId, array('image_file' => $url));
		}
	}

	/**
	* Process Custom Fields
	*
	* @param - $momCF: M.O.M Custom Fields
	*
	* @param - $productId: Product Id collect from the product level, in case there is no custom fields associated with the product yet
	*
	* @param - $api: API client for connecting with the store
	* 
	*/

	public function processCustomFields($productId, $momCF, $api)
	{		
		// $p = $api::getProduct(65);
		// print_r($p->custom_fields);
		// exit();

		$customerId = $this->customer->id;

		$bcCF = $this->db->query('
			SELECT bc_id, product_id, name, text
			FROM bigbackup_bc_products_custom_fields
			WHERE customer_id = :customer_id
			AND product_id = :product_id'
			,
			array(
				'customer_id' => $customerId,
				'product_id' => $productId
			)
		);

		foreach ($momCF as $name => $text) {
			$momCustomFields[] = array (
				'name' => $name,
				'text' => $text,
			);
		}

		$bcCustomFieldsNames = array_column($bcCF, 'name');
		$momCustomFieldsNames = array_column($momCustomFields, 'name');

		$allFields = array_unique(array_merge($bcCustomFieldsNames,$momCustomFieldsNames));
		$fieldsForCreation = array_diff($momCustomFieldsNames, $bcCustomFieldsNames);
		$fieldsForDelete = array_diff($bcCustomFieldsNames, $momCustomFieldsNames);
		$fieldsForUpdate = array_diff(array_diff($allFields, $fieldsForCreation), $fieldsForDelete);


		// print_r($fieldsForCreation);
		// print_r($fieldsForDelete);
		// print_r($fieldsForUpdate);	
		// exit();

// CREATE CUSTOM FIELDS SECTIOIN

		if ( $fieldsForCreation != null) {
		  // Prepare for creating custom fields
			foreach ( $fieldsForCreation as $name ) {
				foreach ( $momCustomFields as $f ) {
					if ( $name == $f['name'] ) {
						// Call the api for creating the custom fields
						$api::createProductCustomField($productId, array(
							'name' => $f['name'],
							'text' => $f['text']
						));	
						print_r($api::getLastError());
						break;
					}
				} // End of inner loop of custom fields creation
			} // End of outter loop of custom fields creation
		} // End of the custom field creation if statement


// UPDATE CUSTOM FIELDS SECTIOIN

		if ( $fieldsForUpdate != null ) {
			
			$diff = array();
			foreach( $bcCF as $B_field) {

				foreach ($momCustomFields as $M_field) {
					
					if ( $B_field['name'] == $M_field['name'] ) {

						if ( $B_field['text'] != $M_field['text'] ) {
							
							$diff[] = array(
								'name' => $B_field['name'],
								'text' => $M_field['text'],
								'product_id' => $B_field['product_id'],
								'id' => $B_field['bc_id']
							);
						}
						break; // if the name matches we will need to break the inner loop
					}
				} // End of inner loop

			} // End of outer loop

			// Update the custom fields from the $diff result

			if ( $diff != null) {
				foreach ($diff as $customField) {
					extract($customField);
					$api::updateProductCustomField($productId, $id, array(
						'name' => $name,
						'text' => $text
					));
					print_r($api::getLastError());
				}
			}
		} // End of Update custom fields if statement


// DELETE CUSTOM FIELDS SECTIOIN


		if ( $fieldsForDelete != null) {
			 // Prepare for creating custom fields
			foreach ( $fieldsForDelete as $name ) {
				foreach ( $bcCF as $f ) {
					if ( $name == $f['name'] ) {
						// Call the api for delete the custom fields
						$api::deleteProductCustomField($productId, $f['bc_id']);
						echo 'deleting...';
						print_r($api::getLastError());
						break;
					}
				} // End of inner loop of custom fields delete
			} // End of outter loop of custom fields delete
		}

	} // End of Process Custom Fields function


	/**
	* Process Bulk Pricing 
	*
	* @param - $momBP: M.O.M Bulk Pricing
	*
	* @param - $productId: Product Id collect from the product level, in case there is no bulk pricing has associated with the product yet
	*
	* @param - $api: API client for connecting with the store
	* 
	*/

	public function processBulkPricing($productId, $momBP, $api)
	{		
		$customerId = $this->customer->id;
		
		// $p = $api::getProduct(65);
		// print_r($p->discount_rules);
		// exit();

		$bcBP = $this->db->queryFirst('
			SELECT bc_id, product_id, min, max, type, type_value
			FROM bigbackup_bc_products_discount_rules
			WHERE customer_id = :customer_id
			AND product_id = :product_id'
			,
			array(
				'customer_id' => $customerId,
				'product_id' => $productId
			)
		);

		// Delete bulk pricing if the $momBP is empty
		if ( is_null($momBP) ) {
			$api::deleteResource('/products/' . $productId . '/discount_rules/' . $bcBP['bc_id']);
			return; 
		}

		if ( is_null($bcBP) ) {
			// Create the bulk pricing
			$api::createProductBulkPricingRules($productId, $momBP);

		} else {
			// Update the bulk pricing
			$bcBulkPricing = array (
  		'type_value' => $bcBP['type_value'],
  		'min' => $bcBP['min'],
  		);
		
			$diff = array_diff( $momBP, $bcBulkPricing );
			if ( !isset($diff['type_value']) ) $diff['type_value'] = $bcBP['type_value'];

			if ( $diff != null ) {
				$api::updateResource('/products/' . $bcBP['product_id'] . '/discount_rules/' . $bcBP['bc_id'], $diff);
				print_r($api::getLastError());
			}

		}  	

	} // End of Process Bulk Pricing function

	/**
	 * returns an instance of this class
	 *
	 * @return ProductService
	 */
	public static function getInstance()
	{
		if (is_null(self::$instance)) {
			self::$instance = new self();
		}

		return self::$instance;
	}
}