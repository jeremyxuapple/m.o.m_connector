<!doctype html>
<html>
	<head>
		<style type="text/css">
			* {
				font-family: sans-serif;
			}

			h1, .desc {
				text-align: center;
			}

			#transactions {
				border: 0 none;
				border-bottom: 3px solid #000;
				border-top: 1px solid #000;
				border-collapse: collapse;
				margin: 2em auto 0 auto;
				width: 1000px;
			}

			#transactions th, #transactions td {
				padding: 0.5em 1em;
				text-align: left;
			}

			#transactions th {
				border-bottom: 1px solid #000;
			}

			#transactions td {
				border-bottom: 1px dashed #000;
			}

			#transactions tfoot td {
				border-top: 3px solid #000;
			}
		</style>

		<script type="text/javascript">
			function printReport() {
				window.print();
			}
		</script>
	</head>

	<body onload="printReport()">
		<h1>Transaction Report for <?=$startDate->format('M j, Y'); ?></h1>

		<p class="desc">
			<?php if ($type == 'all') : ?>
				Listed below are all the transactions made on <?=$startDate->format('M j, Y'); ?>.
			<?php elseif ($type == 'payment') : ?>
				Listed below are all the successful payments made on <?=$startDate->format('M j, Y'); ?>.
			<?php elseif ($type == 'error') : ?>
				Listed below are all transactions with errors that occurred on <?=$startDate->format('M j, Y'); ?>.
			<?php else : ?>
				Listed below are all the RMA and refund transactions on <?=$startDate->format('M j, Y'); ?>.
			<?php endif; ?>
		</p>

		<table id="transactions">
			<thead>
				<tr>
					<th>Date</th>
					<th>Order No.</th>
					<th>Type</th>
					<th>Amount</th>
					<th>Payment Method</th>
					<th>Transaction ID</th>

				</tr>
			</thead>
			<tbody>
				<?php
				// PST timezone
				$timezone = new \DateTimeZone('PST');

				/** @var \MiniBC\addons\momconnector\entities\Transaction $transaction */
				foreach($transactions as $transaction) :
					// adjust for timezone difference
					$date = clone $transaction->date;
					$date->setTimezone($timezone);
				?>
					<tr>
						<td><?=$date->format('M j, Y h:i A e'); ?></td>
						<td><?=$transaction->orderId; ?></td>
						<td><?=$transaction->type; ?></td>
						<td>$<?=number_format($transaction->amount, 2); ?></td>
						<td>**** **** **** <?=$transaction->creditCardLast4; ?></td>
						<td><?=$transaction->transactionId; ?></td>
					</tr>

					<?php if (!empty($transaction->error)) : ?>
						<tr>
							<td colspan="6">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Error: <?=$transaction->error; ?></td>
						</tr>
					<?php endif; ?>
				<?php endforeach; ?>
			</tbody>

			<tfoot>
				<tr>
					<td colspan="6">Total Transactions: <?=$count; ?></td>
				</tr>
			</tfoot>
		</table>
	</body>
</html>