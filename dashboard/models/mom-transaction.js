/**
 * M.O.M. Transaction Model
 */
EmberApp.MomTransaction = DS.Model.extend({
    /**
     * order for this transaction
     */
    'order': DS.belongsTo('mom-order'),

    /**
     * transaction id
     */
    'transactionId': DS.attr('string'),

    /**
     * transaction amount
     */
    'amount': DS.attr('number', { 'defaultValue': 0 }),

    /**
     * transaction type (payment, refund)
     */
    'type': DS.attr('string'),

    /**
     * credit card type
     */
    'ccType': DS.attr('string'),

    /**
     * last 4 digits of credit card
     */
    'ccLast4': DS.attr('string'),

    /**
     * credit card expiry month
     */
    'ccExpm': DS.attr('string'),

    /**
     * credit card expiry year
     */
    'ccExpy': DS.attr('string'),

    /**
     * transaction date
     */
    'date': DS.attr('date'),

	/**
	 * error
	 */
	'error': DS.attr('string'),

	/**
	 * transaction items
	 */
    'items': DS.attr()
});

EmberApp.MomTransactionAdapter = EmberApp.MomAPIAdapter.extend({});