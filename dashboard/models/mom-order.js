/**
 * M.O.M. Connector Order Model
 */
EmberApp.MomOrder = DS.Model.extend({
    /**
     * order id in M.O.M.
     */
    'momOrderId': DS.attr('string'),

    /**
     * order total
     */
    'total': DS.attr('number'),

    /**
     * customer first name
     */
    'firstName': DS.attr('string'),

    /**
     * customer last name
     */
    'lastName': DS.attr('string'),

    /**
     * customer email
     */
    'email': DS.attr('string'),

    /**
     * amount outstanding
     */
    'outstanding': DS.attr('number'),

    /**
     * credit card type
     */
    'ccType': DS.attr('string'),

    /**
     * last 4 digits of credit card
     */
    'ccLast4': DS.attr('string'),

    /**
     * credit card expiry month
     */
    'ccExpm': DS.attr('string'),

    /**
     * credit card expiry year
     */
    'ccExpy': DS.attr('string'),

    /**
     * minFraud risk score, 0.10 to 100
     */
    'riskScore': DS.attr('number'),

    /**
     * rc user id
     */
    'userId': DS.attr('number'),

    /**
     * MiniBC payment profile id
     */
    'profileId': DS.attr('number'),

    /**
     * true if the order is on hold
     */
    'onHold': DS.attr('boolean'),

    /**
     * transactions made for this order
     */
    'transactions': DS.hasMany('mom-transaction')
});

EmberApp.MomOrderAdapter = EmberApp.MomAPIAdapter.extend({});