EmberApp.MomTransactionListingComponent = Ember.Component.extend({
    'tagName': 'ul',
    'classNames': [ 'mom-transaction-listing' ],
    'disable': false,
    'showOrderNo': false,
    'reloadAction': 'reload',
    'actions': {
        'refundTransaction': function(transaction, item) {
            item.set('inProgress', true);
            this.set('disable', true);

            var id = transaction.get('id');
            var component = this;

            Ember.$.ajax({
                "url": "/customer/apps/momconnector/payments/" + id + "/refund",
                "type": "GET",
                "dataType": "json"
            }).done(function() {
                // reload order
                component.sendAction('reloadAction');

                component.notifications.show('Payment has been refunded.', 'Refund Successful', 'success');
            }).fail(function(err) {
                var message = "An error occurred when attempting to refund this transaction.";

                if (err.responseJSON && err.responseJSON.message) {
                    message = err.responseJSON.message;
                }

                component.notifications.show(message, 'Failed to refund payment', 'error');
            }).always(function() {
                item.set('inProgress', false);
                component.set('disable', false);
            });
        }
    }
});