EmberApp.MomBatchResultItemComponent = Ember.Component.extend({
    'tagName': 'li',
    'classNames': [ "mom-batch-result-item", "row" ],
    'classNameBindings': [ 'isAlternate:alternate' ],

    'index': 0,

    'isAlternate': function() {
        var index = this.get('index');
        return ( parseInt(index)%2 > 0 );
    }.property('index')
});