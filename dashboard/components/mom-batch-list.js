EmberApp.MomBatchListComponent = Ember.Component.extend({
    'tagName': 'ul',
    'classNames': [ 'mom-batch-list' ],

    'selected': [],
    'chargeAmounts': {},

    'actions': {
        'addOrder': function(orderId) {
            var selected = this.get('selected');

            if (selected.indexOf(orderId) < 0) {
                selected.push(orderId);
            }

            this.set('selected', selected);
        },

        'removeOrder': function(orderId) {
            var selected = this.get('selected');
            var index = selected.indexOf(orderId);

            if (index > -1) {
                // remove order id from selected array
                selected.splice(index, 1);
            }

            this.set('selected', selected);
        },

        'updateAmount': function(orderId, amount) {
            var amounts = this.get('chargeAmounts');

            if (amount == "0.00") {
                delete amounts[orderId];
            } else {
                amounts[orderId] = amount;
            }

            this.set('chargeAmounts', amounts);
        }
    }
});