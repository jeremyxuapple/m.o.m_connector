EmberApp.MomRiskScoreComponent = Ember.Component.extend({
    'tagName': 'span',
    'classNames': [ 'mom-risk-score' ],
    'classNameBindings': [ 'isSafe:safe', 'isWarning:warning', 'isDanger:danger' ],
	'attributeBindings': [ 'title' ],
	'title': 'minFraud Risk Score',

    'isSafe': function() {
        var score = parseFloat(this.get('score'));

        return (score < 5);
    }.property('score'),

    'isWarning': function() {
        var score = parseFloat(this.get('score'));

        return (score > 4.99 && score < 10);
    }.property('score'),

    'isDanger': function() {
        var score = parseFloat(this.get('score'));

        return (score > 9.99);
    }.property('score')
});