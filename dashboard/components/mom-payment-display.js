EmberApp.MomPaymentDisplayComponent = Ember.Component.extend({
    tagName: 'div',
    classNames: [ 'mom-payment-display' ],
    classNameBindings: [ 'inline' ],

    'type': 'Unknown',
    'last4': '****',
    'expm': 'XX',
    'expy': 'XX',

    'cardTypeClass': function() {
        var type = this.get('type');
        if (!type) return 'credit-card';

        type = type.toLowerCase();

        if (type == 'americanexpress') return 'cc-amex';
        if (type == 'visa') return 'cc-visa';
        if (type == 'mastercard') return 'cc-mastercard';
        if (type == 'discover') return 'cc-discover';
        if (type == 'dinersclub') return 'cc-diners-club';
        if (type == 'jcb') return 'cc-jcb';

        return 'credit-card';
    }.property('type')
});