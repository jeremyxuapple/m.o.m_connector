EmberApp.MomTransactionItemComponent = Ember.Component.extend({
    'tagName': 'li',
    'classNames': [ 'mom-transaction-item', 'animated' ],
    'classNameBindings': [ 'alternate', 'paymentRefunded:refunded' ],

    'showTransactionProduct': false,
	'showOrderNo': false,

    'index': 0,

    'disable': false,
    'inProgress': false,
    'refundAction': 'refundTransaction',

	'loadTransactionProduct': function() {
    	var type = this.get('transaction.type');
		var items = this.get('transaction.items');

		if (type != 'return') return false;

		if (items && items.length > 0) return true;

    	return false;
	}.property('transaction.type'),

    'date': function() {
        var dateStr = this.get('transaction.date');
        var date = moment(dateStr);

        return date.format('MMM D, YYYY h:mm A');
    }.property('transaction.date'),

    'alternate': function() {
        return (parseInt(this.get('index')) % 2 > 0);
    }.property('index'),

    'isPayment': function() {
        return (this.get('transaction.type') == 'payment');
    }.property('transaction.type'),

    'isRefund': function() {
        return (this.get('transaction.type') == 'refund');
    }.property('transaction.type'),

    'paymentRefunded': function() {
        var type = this.get('transaction.type');

        return (type == 'refunded');
    }.property('transaction.type'),

    'transactionType': function() {
        var type = this.get('transaction.type');

        if (type == 'payment') return 'Payment';
        if (type == 'refund') return 'Refund';
        if (type == 'refunded') return 'Payment';
		if (type == 'return') return 'RMA';

        return 'Unknown';
    }.property('transaction.type'),

    'didRender': function() {
        this.$().addClass('slideInDown')
    },

    'actions': {
        /*
    	'refundTransaction': function() {
            this.sendAction('refundAction', this.get('transaction'), this);
        },*/

		'toggleProduct': function() {
			var showProduct = this.get('showTransactionProduct');
			this.set('showTransactionProduct', (!showProduct));
		}
    }
});