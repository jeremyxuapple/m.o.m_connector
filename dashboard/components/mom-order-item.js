EmberApp.MomOrderItemComponent = Ember.Component.extend({
    'tagName': 'li',
    'classNames': [ "mom-order-item", "row" ],
    'classNameBindings': ['alternate'],

    'index': 0,

	'hasError': function() {
		var transactions = this.get('order.transactions');

		// no transactions
		if (transactions.length <= 0) return false;

		var latestTransaction = transactions.objectAt(0);
		var transactionError = latestTransaction.get('error');

		return (transactionError && transactionError != '');
	}.property('order.transactions'),

    'orderInMOM': function() {
		var momOrderId = this.get('order.momOrderId');
		return (momOrderId && momOrderId != '0');
	}.property('order.momOrderId'),

    'alternate': function() {
        var index = this.get('index');

        return ((parseInt(index) % 2) > 0);
    }.property('index')
});