EmberApp.MomOrderListComponent = Ember.Component.extend({
    tagName: 'ul',
    classNames: [ 'mom-order-list', 'table-list-2', 'animated' ],

    'didRender': function() {
        this.$().addClass('fadeInUp');
    }
});