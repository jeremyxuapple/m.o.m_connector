EmberApp.MomBatchItemComponent = Ember.Component.extend({
    'tagName': 'li',
    'classNames': [ 'mom-batch-item', 'row' ],
    'classNameBindings': [ 'isAlternate:alternate' ],

    'checkAction': 'addOrder',
    'uncheckAction': 'removeOrder',
    'updateAmountAction': 'updateAmount',

    'isChecked': false,
    'amount': '',
	'index': 0,

	'isAlternate': function() {
    	return ( parseInt(this.get('index')) % 2 > 0);
	}.property('index'),

    'checkboxChanged': function() {
        var isSelected = this.get('isChecked');
        var orderId = this.get('order.order.id');

        if (orderId) {
            var action = isSelected ? 'checkAction' : 'uncheckAction';
            this.sendAction(action, orderId);
        }
    }.observes('isChecked'),

    'amountChanged': function() {
        var amount = this.get('amount');
        var orderId = this.get('order.order.id');

        if (orderId) {
            if (amount && amount != '') {
                amount = parseFloat(amount).toFixed(2);
                this.set('isChecked', true);
            } else {
                amount = "0.00";
            }

            this.sendAction('updateAmountAction', orderId, amount);
        }
    }.observes('amount'),

    'willInsertElement': function() {
        var onHold = this.get('order.order.onHold');
        var orderTotal = this.get('order.order.total');

        if (orderTotal && !onHold) {
            var amount = parseFloat(orderTotal).toFixed(2);
            this.set('amount', amount);
        }
    }
});