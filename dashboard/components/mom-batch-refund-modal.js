EmberApp.MomBatchRefundModalComponent = Ember.Component.extend({
	'tagName': 'div',
	'classNames': [ "remodal", "mom-batch-refund-modal", "mom-modal" ],

	'closeAction': 'closeModal',
	'instance': false,

	'selected': [],
	'amounts': {},

	'isProcessing': false,
	'showBatchSelection': true,

	'batchResult': [],

	'errorMessage': '',

	'validateBatchData': function() {
		var orders = this.get('orders');
		var selected = this.get('selected');
		var amounts = this.get('amounts');

		var result = {
			'error': false,
			'message': '',
			'orders': []
		};

		if (selected.length <= 0) {
			result.error = true;
			result.message = 'Please select an order to process.';

			return result;
		}

		var selectedOrders = orders.filter(function(order) {
			return (selected.indexOf(order.order.get('id')) > -1);
		});

		if (selectedOrders.length <= 0) {
			result.error = true;
			result.message = 'Please select an order to process.';

			return result;
		}

		selectedOrders.forEach(function(order) {
			var dataRow = {
				'order_id': order.order.get('id'),
				'amount': order.amount,
				'items': order.items
			};

			result.orders.push(dataRow);
		});

		return result;
	},

	'actions': {
		'processBatch': function() {
			var component = this;
			var orders = this.get('orders');
			var store = orders.objectAt(0).order.store;

			this.set('isProcessing', true);

			var batchData = this.validateBatchData();

			if (batchData.error) {
				return this.set('errorMessage', batchData.message).set('isProcessing', false);
			}

			this.set('errorMessage', '');

			// submit to backend for processing
			var result = Ember.$.ajax({
				'url': '/customer/apps/momconnector/payments/batch/refunds',
				'type': 'POST',
				'dataType': 'json',
				'data': { 'orders': batchData.orders }
			});

			result.then(function(results) {
				var batchResults = [];

				results.result.forEach(function(result) {
					var order = store.peekRecord('mom-order', result.id);

					batchResults.pushObject(Ember.Object.create({
						'id': order.get('id'),
						'momOrderId': order.get('momOrderId'),
						'amount': result.amount,
						'success': result.success,
						'message': result.message
					}));
				});

				// show batch result
				component
					.set('batchResult', batchResults)
					.set('showBatchSelection', false);
			}, function(err) {
				// show error

			}).always(function() {
				component.set('isProcessing', false);
			});
		}
	},

	'didInsertElement': function() {
		var inst = this.$().remodal({
			hashTracking: false,
			closeOnOutsideClick: false
		});

		inst.open();

		this.set('instance', inst);

		var controller = this;

		this.$().on('closed', function() {
			controller.sendAction('closeAction');
		});
	},

	'willDestroyElement': function() {
		var inst = this.get('instance');
		if (inst) inst.destroy();
	}
});