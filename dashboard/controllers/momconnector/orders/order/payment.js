EmberApp.MomconnectorOrdersOrderPaymentController = Ember.Controller.extend({
    'amount': '',
    'inProgress': false,

    'actions': {
        'submitPayment': function() {
            var amount = this.get('amount');

            // no amount entered
            if (amount == '') {
                return this.notifications.show('Please enter the payment amount.', 'Error', 'error');
            }

            amount = parseFloat(amount);

            // invalid amount entered
            if (isNaN(amount) || amount <= 0) {
                return this.notifications.show('Please enter a valid payment amount.', 'Error', 'error');
            }

            var outstanding = parseFloat(this.get('model.outstanding'));

            if (amount > outstanding) {
                return this.notifications.show(
                    'The payment amount cannot be greater than the outstanding amount on this order.',
                    'Error',
                    'error'
                );
            }

            this.set('inProgress', true);

            var controller = this;
            var orderId = this.get('model.id');
            var data = {
                'order_id': orderId,
                'amount': amount
            };

            Ember.$.ajax({
                "url": "/customer/apps/momconnector/payments",
                "type": "POST",
                "data": data,
                "dataType": "json"
            }).done(function() {
                controller.notifications.show('Payment processed successfully.', 'Payment Processed', 'success');

                // reload order and transaction
                controller.store.findRecord('mom-order', orderId, { reload: true });

                controller.transitionTo('momconnector.orders.order', orderId);
            }).fail(function(err) {
                var message = 'An error occurred when processing your payment.';

                if (err.responseJSON && err.responseJSON.message) {
                    message = err.responseJSON.message;
                }

                controller.notifications.show(message, 'Failed to Process Payment', 'error');
            }).always(function() {
                controller.set('inProgress', false);
            });
        }
    }
});