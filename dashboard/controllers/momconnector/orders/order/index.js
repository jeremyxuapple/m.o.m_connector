EmberApp.MomconnectorOrdersOrderIndexController = Ember.Controller.extend({
    'hasOutstanding': function() {
        var outstanding = this.get('model.outstanding');

        if (!outstanding) return false;

        return (parseFloat(outstanding) > 0);
    }.property('model.outstanding'),

    'orderInMOM': function() {
		var momOrderId = this.get('model.momOrderId');
		return (momOrderId && momOrderId != '0');
	}.property('model.momOrderId'),

    'actions': {
        'reload': function() {
            this.store.findRecord('mom-order', this.get('model.id'), { 'reload': true });
        }
    }
});