EmberApp.MomconnectorOrdersOrderRefundController = Ember.Controller.extend({
	'amount': '',
	'inProgress': false,

	'actions': {
		'processRefund': function() {
			var amount = this.get('amount');

			// no amount entered
			if (amount == '') {
				return this.notifications.show('Please enter the refund amount.', 'Error', 'error');
			}

			amount = parseFloat(amount);

			// invalid amount entered
			if (isNaN(amount) || amount <= 0) {
				return this.notifications.show('Please enter a valid refund amount.', 'Error', 'error');
			}

			var orderTotal = parseFloat(this.get('model.total'));

			if (amount > orderTotal) {
				return this.notifications.show(
					'The payment amount cannot be greater than the order total.',
					'Error',
					'error'
				);
			}

			this.set('inProgress', true);

			var controller = this;
			var orderId = this.get('model.id');
			var data = {
				'order_id': orderId,
				'amount': amount
			};

			Ember.$.ajax({
				"url": "/customer/apps/momconnector/refunds",
				"type": "POST",
				"data": data,
				"dataType": "json"
			}).done(function() {
				controller.notifications.show('Refund issued successfully.', 'Refund Successful', 'success');

				// reload order and transaction
				controller.store.findRecord('mom-order', orderId, { reload: true });

				controller.transitionTo('momconnector.orders.order', orderId);
			}).fail(function(err) {
				var message = 'An error occurred when processing your refund.';

				if (err.responseJSON && err.responseJSON.message) {
					message = err.responseJSON.message;
				}

				controller.notifications.show(message, 'Failed to Process Refund', 'error');
			}).always(function() {
				controller.set('inProgress', false);
			});
		}
	}
});