EmberApp.MomconnectorOrdersIndexController = Ember.Controller.extend({
    'searchTerm': '',

    'syncInProgress': false,

    'isLoading': false,
    'isSearchResult': false,

    'showBatchModal': false,
	'showRefundModal': false,

    'pages': 0,
    'currentPage': 1,

    'batchOrders': [],

	'searchDelay': 500,
    'timeout': false,

    'searchTermChanged': function() {
        this.set('isLoading', true);

        var timeout = this.get('timeout');
        var delay = this.get('searchDelay');

        // cancel existing timeout
        if (timeout) Ember.run.cancel(timeout);

        timeout = Ember.run.later(this, function() {
            var controller = this;

            controller.set('currentPage', 1);

            this.updateModel()
                .finally(function() {
                    controller.set('isLoading', false);
                });
        }, delay);

        this.set('timeout', timeout);
    }.observes('searchTerm'),

    'updateModel': function() {
        var term = this.get('searchTerm');

        if (term && term != '') {
            this.set('isSearchResult', true);
        } else {
            this.set('isSearchResult', false);
        }

        var page = this.get('currentPage');
        var controller = this;

        return new Ember.RSVP.Promise(function(resolve, reject) {
            controller.store.query('mom-order', { 'term': term, 'page': page })
                .then(function(orders) {
                    var pages = orders.get('meta.pages');

                    if (pages) {
                        pages = parseInt(pages);
                    } else {
                        pages = 0;
                    }

                    controller
                        .set('pages', pages)
                        .set('model', orders)
                        .set('isError', false);

                    resolve(orders);
                }, function(err) {
                    controller.set('isError', true);

                    reject(err);
                });
        });
    },

    'getBatchOrders': function(respData) {
        var controller = this;

        return new Ember.RSVP.Promise(function(resolve, reject) {
            var orders = [];
            var promises = [];

			respData.forEach(function(order) {
            	console.log('getting order data for order no. ' + order.id);

                var getOrder = controller.store.findRecord('mom-order', order.id, { reload: true })
                    .then(function(momOrder) {
                        var orderData = { 'amount': order.amount, 'order': momOrder, 'items': [] };
                        if (order.items) {
                        	orderData.items = order.items;
						}

                        orders.pushObject(orderData);
                    });

                promises.pushObject(getOrder);
            });

            Ember.RSVP.all(promises).then(function() {
                controller.set('batchOrders', orders);

                resolve();
            }, function() {
                reject();
            });
        });
    },

    'actions': {
        'downloadFromMOM': function(type) {
			var controller = this,
				url = '/customer/apps/momconnector/update';

        	this.set('syncInProgress', true).set('batchOrders', []);

			if (type == 'refund') {
				url = '/customer/apps/momconnector/update/refunds';
			}

            Ember.$.ajax({
                'url': url,
                'type': 'GET',
                'dataType': 'json'
            }).done(function(resp) {
                // gather data for new orders
                controller.getBatchOrders(resp.orders)
                    .then(function() {
                    	if (type == 'refund') {
							controller
								.set('showRefundModal', true)
								.set('showBatchModal', false);
						} else {
							controller
								.set('showRefundModal', false)
								.set('showBatchModal', true);
						}
                    }, function() {
                        controller.notifications.show(
                            'Failed to load orders for the current batch.',
                            'Failed to Load Orders',
                            'error'
                        );
                    })
                    .finally(function() {
                    controller.set('syncInProgress', false);
                });
            }).fail(function(err) {
                var message = 'Failed to download new orders from M.O.M. Please ensure that the web server is running.';

                if (err.responseJSON && err.responseJSON.message) {
                    message = err.responseJSON.message;
                }

                controller.notifications.show(message, 'Failed to get new orders.', 'error');
                controller.set('syncInProgress', false);
            });
        },

        'changePage': function(page) {
            var controller = this;

            this
				.set('changingPage', true)
				.set('currentPage', page)
				.updateModel()
                .finally(function() {
                    controller.set('changingPage', false);
                });
        },

        'closeModal': function() {
            var controller = this;

            this.set('isLoading', true)
                .set('currentPage', 1)
                .set('showBatchModal', false)
				.set('showRefundModal', false)
                .updateModel()
                .then(function() {
                    controller.notifications.show(
                        'New orders have been downloaded from M.O.M.',
                        'Success',
                        'success'
                    );
                })
                .finally(function() {
                    controller.set('isLoading', false);
                });
        }
    }
});