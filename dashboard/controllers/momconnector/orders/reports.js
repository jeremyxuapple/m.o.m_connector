EmberApp.MomconnectorReportsController = Ember.Controller.extend({
	'isLoading': false,

	'pages': 0,
	'currentPage': 1,

	'filterTypes': [
		{ 'id': 'all', 'label': 'All', 'selected': true },
		{ 'id': 'payment', 'label': 'Payments', 'selected': false },
		{ 'id': 'refund', 'label': 'Refunds', 'selected': false },
		{ 'id': 'error', 'label': 'Errors', 'selected': false }
	],

	'selectedType': 'all',

	'searchDelay': 500,
	'timeout': false,

	'currentDate': '',
	'selectedDate': '',

	'dateChanged': function() {
		this.set('isLoading', true);

		var timeout = this.get('timeout');
		var delay = this.get('searchDelay');

		// cancel existing timeout
		if (timeout) Ember.run.cancel(timeout);

		timeout = Ember.run.later(this, function() {
			var controller = this;

			controller.set('currentPage', 1);

			this.updateModel()
				.finally(function() {
					controller.set('isLoading', false);
				});
		}, delay);

		this.set('timeout', timeout);
	}.observes('selectedDate'),

	'updateModel': function() {
		var page = this.get('currentPage');
		var type = this.get('selectedType');
		var date = this.get('selectedDate');

		var controller = this;

		return new Ember.RSVP.Promise(function(resolve, reject) {
			controller.store.query(
				'mom-transaction',
				{
					'page': page,
					'type': type,
					'date': date
				}
			)
			.then(function(transactions) {
				var pages = transactions.get('meta.pages');

				if (pages) {
					pages = parseInt(pages);
				} else {
					pages = 0;
				}

				controller
					.set('pages', pages)
					.set('model', transactions)
					.set('isError', false);

				resolve(transactions);
			}, function(err) {
				controller.set('isError', true);

				reject(err);
			});
		});
	},

	'actions': {
		'changeType': function(type) {
			this.set('isLoading', true).set('selectedType', type);

			var timeout = this.get('timeout');
			var delay = this.get('searchDelay');

			// cancel existing timeout
			if (timeout) Ember.run.cancel(timeout);

			timeout = Ember.run.later(this, function() {
				var controller = this;

				controller.set('currentPage', 1);

				this.updateModel()
					.finally(function() {
						controller.set('isLoading', false);
					});
			}, delay);

			this.set('timeout', timeout);
		},

		'changePage': function(page) {
			var controller = this;

			this
				.set('changingPage', true)
				.set('currentPage', page)
				.updateModel()
				.finally(function() {
					controller.set('changingPage', false);
				});
		}
	}
});