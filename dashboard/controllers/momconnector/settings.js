EmberApp.MomconnectorSettingsController = Ember.Controller.extend({
	'minFraudLicense': '',
	'apiEndpoint': '',

	'actions': {
		'save': function() {
			var controller = this;
			this.set('inProgress', true);

			Ember.$.ajax({
				'url': '/customer/apps/momconnector/settings',
				'type': 'POST',
				'data': {
					'license_key': this.get('minFraudLicense'),
					'apiEndpoint': this.get('apiEndpoint')
				},
				'dataType': 'json'
			}).done(function() {
				controller.notifications.show('Settings saved.', 'Success', 'success');
			}).fail(function() {
				controller.notifications.show('Failed to save settings.', 'Error', 'error');
			}).always(function() {
				controller.set('inProgress', false);
			});
		}
	}
});