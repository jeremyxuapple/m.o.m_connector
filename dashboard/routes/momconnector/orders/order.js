EmberApp.MomconnectorOrdersOrderRoute = Ember.Route.extend({
    'model': function(params) {
        return this.store.findRecord('mom-order', params.id, { reload: true });
    }
});