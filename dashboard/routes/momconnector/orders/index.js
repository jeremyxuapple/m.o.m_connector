EmberApp.MomconnectorOrdersIndexRoute = Ember.Route.extend({
    'model': function() {
        return this.store.query('mom-order', {});
    },

    'setupController': function(controller, model) {
        this._super(controller, model);

        var pages = model.get('meta.pages');

        if (pages) {
            controller.set('pages', parseInt(pages));
        }
    }
});