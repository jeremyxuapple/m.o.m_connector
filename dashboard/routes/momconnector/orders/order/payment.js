EmberApp.MomconnectorOrdersOrderPaymentRoute = Ember.Route.extend({
    afterModel: function(model) {
		var momOrderId = model.get('momOrderId');

		if (!momOrderId || momOrderId == '0') {
			this.transitionTo('momconnector.orders.order', model.get('id'));
		}
	},

    setupController: function(controller, model) {
        this._super(controller, model);

        controller.set('amount',model.get('outstanding'));
    }
});