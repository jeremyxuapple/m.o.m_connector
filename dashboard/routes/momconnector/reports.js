EmberApp.MomconnectorReportsRoute = Ember.Route.extend({
	'model': function() {
		return this.store.query('mom-transaction', {});
	},

	'setupController': function(controller, model) {
		this._super(controller, model);

		var pages = model.get('meta.pages');

		if (pages) {
			controller.set('pages', parseInt(pages));
		}

		var now = moment().format('YYYY-MM-DD');
		controller
			.set('selectedType', 'all')
			.set('selectedDate', now)
			.set('currentDate', now);
	}
});