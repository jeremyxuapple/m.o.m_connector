EmberApp.MomconnectorSettingsRoute = Ember.Route.extend({
	'model': function() {
		return Ember.$.ajax({
			'url': '/customer/apps/momconnector/settings',
			'type': 'GET',
			'dataType': 'json'
		});
	},

	'setupController': function(controller, settings) {
		this._super(controller, settings);

		if (settings.minfraud_license)  {
			controller.set('minFraudLicense', settings.minfraud_license);
		}

		if (settings.api_endpoint) {
			controller.set('apiEndpoint', settings.api_endpoint);
		}
	}
});