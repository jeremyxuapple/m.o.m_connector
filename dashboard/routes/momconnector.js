EmberApp.MomconnectorRoute = Ember.Route.extend({
    model: function() {
        return this.store.find('app', 'momconnector');
    },
    afterModel: function(model) {
        var appsController = this.controllerFor('apps');

        appsController.set('showAppHeader', true);
        appsController.set('activeApp', model);

        var applicationController = this.controllerFor('application');
        applicationController.set('selectedApp', model.get('label'));

        // get menu items
        var menuItems = appMenuItems.get('momconnector');
        applicationController.set('appsMenu', menuItems);
    },

    actions: {
        'downloadFromMOM': function() {
            console.log('download from MOM');
        }
    }
});