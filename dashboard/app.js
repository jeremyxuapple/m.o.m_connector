EmberApp.Router.map(function() {
    this.route('momconnector', { 'path': '/apps/momconnector' }, function() {
        // orders
        this.route('orders', { "path": "/orders" }, function() {
            this.route('order', { "path": "/:id" }, function() {
                this.route('payment', { "path": "/payment" });
                this.route('refund', { "path": "/refund" });
            });
        });

		this.route('reports', { "path": "/reports" });
        this.route('settings', { "path": "/settings" });
    });
});

/**
 * API adapter for this app
 */
EmberApp.MomAPIAdapter = DS.JSONAPIAdapter.extend({
    "namespace": "customer/apps/momconnector"
});