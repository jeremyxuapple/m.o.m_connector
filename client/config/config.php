<?php 
/**
 * Client Configuration
 */

// MiniBC constants
define('MBC_STORE_HASH', '');							// MiniBC Store Hash
define('BIZSYNC_ORDER_TYPE', 'NET');					// default BizSync order type (NET)
define('BIZSYNC_PAYMENT_METHOD', 'CK');					// default payment method for BizSync
define('MOM_WEB_USER', 'WEB');							// MOM user for web orders

// order item status that identifies the order as ready for payment
define('PAYMENT_READY_ITEM_STATUS', 'PI');
define('PAYMENT_ALT_READY_ITEM_STATUS', 'SV');
define('DROPSHIP_READY_ITEM_STATUS', 'ND');

// order item status that identifies the order as an return
define('PAYMENT_REFUND_ITEM_STATUS', 'RT');

// M.O.M. SDK Configuration
define('sys_MOMVersion', 	'PREMIUM');					//M.O.M. Edition, STANDARD or PREMIUM

define('sys_MOM_UID', 		'');						//M.O.M. User ID
define('sys_MOM_PWD', 		'');						//M.O.M. Password

// Required for M.O.M. Standard Edition only
define('sys_MOMDATAPath',	'C:\\MOMLocal8\\data\\');	//M.O.M. Data Path
define('sys_MOMSHAREPath', 	'C:\\MOMLocal8\\data\\');	//M.O.M. Shared Path

// Required for M.O.M. Premium Edition only
define('sys_SQL_database',	'');						//SQL Database Name
define('sys_SQL_Server', 	'');						//SQL Host
define('sys_SQL_UID',		'');						//SQL Username
define('sys_SQL_PWD',		'');						//SQL Password
define('sys_SQL_Trusted',	'yes');						//Trusted Connection