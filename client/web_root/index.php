<?php
require_once(__DIR__ . '/functions.php');
require_once(__DIR__ . '/classes/RequestController.php');

global $ox;

register_shutdown_function('cleanup');

// check auth header
$headers = getallheaders();

// missing auth header or invalid store hash
if (empty($headers['X-MBC-AUTH']) || $headers['X-MBC-AUTH'] != MBC_STORE_HASH) {
	http_response_code(403);
	exit;
}

// connect to M.O.M. database
momDBConnect();

if (!isset($ox)) {
	logMessage('Failed to establish connection to M.O.M. database.');

	http_response_code(500);
	exit;
}

populateInputData();

// missing API action
if (empty($_POST['action'])) {
	http_response_code(400);
	exit;
}

/** @var string $action API action */
$action = strtolower($_POST['action']);
$controller = new RequestController();

/**
 * route actions
 */
switch ($action) {
	/**
	 * get MOM order id to match with a BC order id
	 */
	case 'get_orders':
		$controller->getMOMOrders();
		break;

	case 'get_refunds':
		$controller->getMOMRefunds();
		break;

	/**
	 * update with new transaction information
	 */
	case 'push_transaction':
		// missing transaction information
		if (empty($_POST['transaction'])) {
			http_response_code(400);
			exit;
		}

		$controller->updateTransaction($_POST['transaction']);
		break;

	/**
	 * invalid action
	 */
	default:
		// invalid action
		http_response_code(400);
		exit;
}

exit;