<?php

require_once(realpath(__DIR__ . '/../connect.php'));
require_once(realpath(__DIR__ . '/../functions.php'));

class ProductsController {
	/** @var COM $ox MOM API connection */
	protected $api;

	/**
	* constructor
	*/
	public function __construct()
	{	
		/* Connect to a Microsoft SQL database using driver invocation */
		// $dsn = 'mysql:host=elusive001; dbname=BigCommerce_MailOrderManager';
		// $user = 'beapartof';
		// $password = 'Rp^52ntM=H';

		// try {
		// 	$dbh = new PDO($dsn, $user, $password);
		// } catch (PDOException $e) {
		// 	echo 'Connection failed: ' . $e->getMessage();
		// }
		
		// exit();
	}
	
	public function getMOMProducts()
	{		
		global $ox;
		global $rsrlink;
				
		$iBatchCount = 100;
		$iTotalRecords = 0;
		$Result = 0;					//store value return from API Call
		$cWhere = "NUMBER like 'AB%'";  	//items that begin with AB
		$MOMProducts = array();

		if (isset($ox)==TRUE)
		{
			$Result = $ox->Stock_Set_Data_Pull($cWhere);
	
			if ($Result < 1)
			{
				// Handle Error
				echo $ox->ERROR_MSG;
				die;
			}
		
			$iTotalRecords = $Result;
		
			//set the number of records to be returned
			$ox->RETURN_ROWS = $iBatchCount;
		
			$iCntd = ceil($iTotalRecords / $iBatchCount);
		
			for($i=0; $i<=$iCntd -1; $i++)
			{
				if (empty($ox->XML))
					break;
				
				$Result = $ox->Stock_Get_Data_Pull();
				if ($Result < 1)
				{
					echo $ox->ERROR_MSG;
					die;
				}
			// convert XML object to json
				$xml = simplexml_load_string($ox->XML);
				$json = json_encode($xml);
				$array = json_decode($json,TRUE);
			
				foreach($array['sqlresult'] as $p) {
					array_push($MOMProducts, $p);
				}
					
			}
			// print_r($MOMProducts);
			$this->sendJSONResponse($MOMProducts);
			exit();
			
		}
		else
		{
			echo "Connection Not Established";
			die;
		}	
		unset($ox);
	} // End of the getMOMProducts function
	
	/**
	 * send JSON response
	 *
	 * @param mixed $data
	 */
	public function sendJSONResponse($data)
	{
		header('Content-Type: application/json');
		echo json_encode($data);
		exit;
	}
	
} // End of ProductsController

