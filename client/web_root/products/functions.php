<?php
require_once(__DIR__ . '/connect.php');

/**
 * connect to M.O.M. database
 *
 * @return \stdClass|string
 */
function momDBConnect()
{
	global $rsrlink;
	global $ox;
	
	try {
		
		/* $ox = new COM("MOM_API.momfunctions"); */
		
		$ox = new COM("mom9_api.MOMFUNCTIONS");
		
		if (isset($ox)!= TRUE) {
			logMessage('Unable to create object');
			exit;
		}

		// establish database connection
		if (sys_MOMVersion == 'STANDARD') {
			$rsrlink = $ox->DB_Connect_VFP(sys_MOMDATAPath, sys_MOMSHAREPath);
		} else {
			$sys_SQL_ConnString = 'Driver=SQL Server;UID=' . sys_SQL_UID . ';Database=' . sys_SQL_database . ';server=' . sys_SQL_Server . ';PWD=' . sys_SQL_PWD . ';trusted_connection=' . sys_SQL_Trusted;
			$rsrlink = $ox->DB_Connect_SQL($sys_SQL_ConnString);
		}

		if($rsrlink > 0) {	
			//connection established
			//attempt to log in
			$loginValue = $ox->log_in(sys_MOM_UID, sys_MOM_PWD);			
			
			if($loginValue == -1) {
				logMessage($ox->ERROR_MSG);
				unset($ox);
				return "Unable to log in to MOM System";
			}
		} else {
			//Handle Error
			logMessage($ox->ERROR_MSG);
			unset($ox);

			return "Unable to connect to MOM System";
		}

		return $rsrlink;
	} catch (com_exception $e) { 
		logMessage($e->getMessage());
		exit;
	}		
}

/**
 * populate POST array with input data
 */
function populateInputData()
{
	// attempt to fill the POST array with raw JSON post data
	$fh = fopen('php://input', 'r');
	$data = '';

	while (($chunk = fread($fh, 2056)) !== false && !empty($chunk)) {
		$data .= $chunk;
	}

	fclose($fh);

	$data = json_decode($data, true);
	if (is_array($data)) $_POST = array_merge($_POST, $data);
}

/**
 * log message
 *
 * @param string 	$message message to log
 * @param array 	$context array context
 */
function logMessage($message, $context = array())
{
	$logPath = __DIR__ . '/log.txt';

	$fh = fopen($logPath, 'a+');

	if ($fh) {
		$now = new \DateTime();
		$logMessage = sprintf('[%s] %s', $now->format('Y-m-d H:i:s'), $message) . PHP_EOL;
		
		if (!empty($context)) {
			$logMessage .= print_r($context, true) . PHP_EOL;
		}

		fwrite($fh, $logMessage);
		fclose($fh);
	}
}

/**
 * cleanup variables
 */
function cleanup()
{
	global $ox;
	unset($ox);
}