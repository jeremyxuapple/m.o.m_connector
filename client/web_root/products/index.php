<?php
require_once(__DIR__ . '/functions.php');
require_once(__DIR__ . '/classes/ProductsController.php');

// phpinfo();
// exit();

global $ox;
   
register_shutdown_function('cleanup');

// check auth header
$headers = getallheaders();

// missing auth header or invalid store hash
/* if (empty($headers['X-MBC-AUTH']) || $headers['X-MBC-AUTH'] != MBC_STORE_HASH) {
	http_response_code(403);
	exit;
} */

// connect to M.O.M. database
momDBConnect();

if (!isset($ox)) {
	logMessage('Failed to establish connection to M.O.M. database.');
	http_response_code(500);
	exit;
}

$controller = new ProductsController();

$controller->getMOMProducts();

exit;