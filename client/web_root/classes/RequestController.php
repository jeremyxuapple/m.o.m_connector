<?php
require_once(__DIR__ . '/../../../config/config.php');

/**
 * RequestController
 */
class RequestController
{
	const BATCH_SIZE = 10;

	/** @var COM $ox MOM API connection */
	protected $api;

	/**
	 * constructor
	 */
	public function __construct()
	{
		global $ox;
		$this->api = $ox;
	}

	/**
	 * get MOM orders that are ready to be shipped
	 */
	public function getMOMOrders()
	{
		$response = array( 'orders' => array() );
		$orders = $this->getOrders();

		foreach ($orders as $order) {
			$total = $order['amount'] + $order['shipping'];
			if ($total > $order['ord_total']) $total = $order['ord_total'];

			$response['orders'][] = array(
				'bc_order_id'	=> $order['bc_order_id'],
				'mom_order_id'	=> $order['id'],
				'amount'		=> $order['amount'],
				'sv_amount'		=> $order['sv_amount'],
				'shipping'		=> $order['shipping'],
				'total'			=> $total,
				'order_total'	=> $order['ord_total'],
				'hold'			=> $order['hold']
			);
		}

		$this->sendJSONResponse($response);
	}

	/**
	 * returns a list of order items that are marked as returns
	 */
	public function getMOMRefunds()
	{
		$response = array( 'orders' => array() );

		// get returned orders
		$refunds = $this->getRefunds();

		foreach ($refunds as $refund)
		{
			$response['orders'][] = array(
				'bc_order_id'	=> $refund['bc_order_id'],
				'mom_order_id'	=> $refund['id'],
				'items'			=> $refund['items']
			);
		}

		$this->sendJSONResponse($response);
	}

	/**
	 * update order with new transaction id
	 *
	 * @param array $transaction
	 */
	public function updateTransaction($transaction = array())
	{
		$response = array( 'status' => 0 );

		$orderId = (int)$transaction['mom_order_id'];

		// lock order
		$result = $this->api->Order_Modify($orderId);

		if ($result < 1) {
			$message = 'Failed to lock order for exclusive use.';

			logMessage($message, array( 'order_id' => $orderId ));

			$response['status'] = 0;
			$response['message'] = $message;

			$this->sendJSONResponse($response);
		}

		// update with transaction id
		$result = $this->api->Order_Payment_CK($orderId, $transaction['transaction_id'], (float)$transaction['amount']);

		if ($result < 1) {
			$message = $this->api->ERROR_MSG;

			logMessage($message, $transaction);

			$response['status'] = 0;
			$response['message'] = $message;

			$this->sendJSONResponse($response);
		}

		// commit changes
		if (!$this->api->SAVE_PARTIAL_ORDERS) {
			$result = $this->api->Order_Save($orderId);

			if ($result < 1) {
				$message = 'Failed to commit order changes.';

				logMessage($message, $transaction);

				$response['status'] = 0;
				$response['message'] = $message;

				$this->sendJSONResponse($response);
			}
		}

		$response['status'] = 1;
		$this->sendJSONResponse($response);
	}

	/**
	 * returns the MOM order ID for a given BC ID
	 *
	 * @return array|bool returns false if order not found
	 */
	public function getMOMOrder($bcOrderId)
	{
		// grab orders that are incomplete
		$where = "ordertype = '" . BIZSYNC_ORDER_TYPE . "' AND alt_order = '" . $bcOrderId . "'";
		$results = $this->api->Order_Set_Data_Pull($where);

		// order not found
		if ($results < 1) return false;

		$this->api->RETURN_ROWS = 1;

		// order not found
		if (empty($this->api->XML)) return false;

		$batchResults = $this->api->Order_Get_Data_Pull();

		// order not found
		if ($batchResults < 1) return false;

		$xmlStr = $this->api->XML;

		// parse XML
		$xml = new SimpleXMLElement( $xmlStr );

		$result = $xml->sqlresult[0];

		$orderId = (int)$result->orderno->__toString();

		if (!$this->orderHasItems($orderId, PAYMENT_READY_ITEM_STATUS)) {
			return false;
		}

		return array(
			'id' 	=> (int)$result->orderno,
			'hold'	=> ($result->perm_hold == 'true')
		);
	}

	public function getOrders()
	{
		$orders = array();

		// grab orders that have been returned
		$where = "ordertype = '" . BIZSYNC_ORDER_TYPE . "' AND userid = '" . MOM_WEB_USER . "' AND paymethod = '" . BIZSYNC_PAYMENT_METHOD . "'";
		$where = "ordertype = '" . BIZSYNC_ORDER_TYPE . "' AND paymethod = '" . BIZSYNC_PAYMENT_METHOD . "'";
		$resultCount = $this->api->Order_Set_Data_Pull($where);

		// API error
		if ($resultCount < 1) return false;

		// no data found
		if ($resultCount == 0) return $orders;

		// set number of rows to return
		$this->api->RETURN_ROWS = self::BATCH_SIZE;

		// calculate number of pages
		$pages = ceil( (int)$resultCount / self::BATCH_SIZE );

		for ($current = 1; $current <= $pages; $current++) {
			$result = $this->api->Order_Get_Data_Pull();
			$xmlStr = $this->api->XML;

			$strEncoding = mb_detect_encoding($xmlStr, "CP1252, auto");

			// failed to detect encoding
			if ($strEncoding === false) $strEncoding = 'ASCII';

			$xmlStr = mb_convert_encoding($xmlStr, 'Windows-1252', $strEncoding);

			// no content
			if (empty($xmlStr) || is_null($xmlStr)) break;

			// removes UTF8 characters
			$xmlStr = $this->UTF8ForXML($xmlStr);

			// no content
			if (empty($xmlStr) || is_null($xmlStr)) continue;

			try {
				$xml = new SimpleXMLElement( $xmlStr );
			} catch (\Exception $e) {
				// error parsing XML
				return false;
			}

			foreach ($xml->sqlresult as $sqlRow) {
				$bcOrderId = (int)$sqlRow->alt_order->__toString();

				if ($bcOrderId <= 0) continue;

				$orders[] = array(
					'id'			=> (int)$sqlRow->orderno->__toString(),
					'bc_order_id'	=> $bcOrderId,
					'hold'			=> ($sqlRow->perm_hold->__toString() == 'true'),
					'shipping'		=> $this->getOrderShippingCharge($sqlRow),
					'ord_total'		=> (float)$sqlRow->ord_total->__toString()
				);
			}
		}

		foreach ($orders as $i => &$order) {
			$amountData = $this->getOrderTotal($order['id'], array( PAYMENT_READY_ITEM_STATUS, PAYMENT_ALT_READY_ITEM_STATUS, DROPSHIP_READY_ITEM_STATUS ));

			// failed to get order total
			if ($amountData === false) return false;

			$amount = $amountData['amount'];
			$svAmount = $amountData['sv_amount'];

			if ($amount <= 0 && $svAmount <= 0) {
				unset($orders[$i]);
				continue;
			}

			$order['amount'] = $amount;
			$order['sv_amount'] = $svAmount;
		}

		return array_values($orders);
	}

	public function getRefunds()
	{
		$refunds = array();

		// grab orders that have been returned
		$where = "ordertype = '" . BIZSYNC_ORDER_TYPE . "' AND userid = '" . MOM_WEB_USER . "' AND paymethod = '" . BIZSYNC_PAYMENT_METHOD . "'";

		$resultCount = $this->api->Order_Set_Data_Pull($where);

		// API error
		if ($resultCount < 1) return false;

		// no data found
		if ($resultCount == 0) return $refunds;

		// set number of rows to return
		$this->api->RETURN_ROWS = self::BATCH_SIZE;

		// calculate number of pages
		$pages = ceil( (int)$resultCount / self::BATCH_SIZE );

		for ($current = 1; $current <= $pages; $current++) {
			$result = $this->api->Order_Get_Data_Pull();
			$xmlStr = urldecode($this->api->XML);

			// no content
			if (empty($xmlStr) || is_null($xmlStr)) {
				continue;
			}

			// removes UTF8 characters
			$xmlStr = $this->UTF8ForXML($xmlStr);

			// no content
			if (empty($xmlStr) || is_null($xmlStr)) {
				continue;
			}

			try {
				$xml = new SimpleXMLElement( $xmlStr );
			} catch (\Exception $e) {
				// error parsing XML
				return false;
			}

			foreach ($xml->sqlresult as $sqlRow) {
				$bcOrderId = (int)$sqlRow->alt_order->__toString();

				// missing Bigcommerce order id
				//if ($bcOrderId <= 0) continue;

				$refunds[] = array(
					'id'			=> (int)$sqlRow->orderno->__toString(),
					'bc_order_id'	=> $bcOrderId,
					'items'			=> array()
				);
			}
		}

		foreach ($refunds as $i => &$refund) {
			$items = $this->getOrderItems($refund['id'], array( PAYMENT_REFUND_ITEM_STATUS ));

			// failed to get order total
			if ($items === false) return false;

			$refund['items'] = $items;
		}

		$refunds = array_filter($refunds, function($refund) {
			return (count($refund['items']) > 0);
		});

		return array_values($refunds);
	}

	function getOrderShippingCharge(SimpleXMLElement $order)
	{
		$shippedItems = (float)$order->nship->__toString();

		/**
		 * full shipping charge is added on first shipment,
		 * return 0 for subsequent shipments
		 */
		if ($shippedItems > 0) return 0;

		$cost = (float)$order->shipping->__toString();

		// no shipping charge
		if ($cost == 0) return $cost;

		$chargeTax = ($order->taxship->__toString() != 'false');

		if ($chargeTax) {
			// amount of tax we need to charge
			$taxRate = $this->getTaxRate($order);

			$cost = $cost * ( 1 + ( $taxRate / 100 ) );
		}

		return $cost;
	}

	function getTaxRate($order)
	{
		$taxRates = array();
		$taxRates[] = (float)$order->ntaxrate->__toString();
		$taxRates[] = (float)$order->staxrate->__toString();
		$taxRates[] = (float)$order->ctaxrate->__toString();
		$taxRates[] = (float)$order->itaxrate->__toString();

		// amount of tax we need to charge
		return array_sum($taxRates);
	}

	function getOrderTotal($id, $itemStatuses)
	{
		// invalid format for item statuses
		if (!is_array($itemStatuses)) return false;

		$total = 0;
		$svTotal = 0;

		$statusFound = false;
		$results = $this->api->OrdDetail_Get($id);

		if ($results < 1) return false;

		try {
			$xmlStr = $this->UTF8ForXML($this->api->XML);
			$xml = new SimpleXMLElement($xmlStr);

			foreach ($xml->sqlresult as $result) {
				// item found
				$status = $result->item_state->__toString();

				if (in_array($status, $itemStatuses)) {
					$statusFound = true;
					$amount = (float)$result->it_unlist->__toString();
					$discount = (int)$result->discount->__toString();

					$qty = (float)$result->quantf->__toString();

					if ($status == 'SV') {
						// service Item are ordered and not filled
						$qty = (float)$result->quanto->__toString();
					}

					if (in_array(PAYMENT_REFUND_ITEM_STATUS, $itemStatuses)) {
						$qty = (float)$result->quanto->__toString();
					}

					$amount = $amount * $qty;

					// reduce amount based on discount
					if ($discount > 0) {
						$amount = $amount * ( 1 - ($discount / 100) );
					}

					// tax
					$taxRate = $this->getTaxRate($result);
					$amount = $amount * ( 1 + ( $taxRate / 100 ) );

					if ($status == 'SV') {
						$svTotal += $amount;
					} else {
						$total += $amount;
					}
				}
			}
		} catch (\Exception $e) {
			return false;
		}

		if (!$statusFound && in_array(PAYMENT_REFUND_ITEM_STATUS, $itemStatuses)) {
			return array( 'amount' => 1, 'sv_amount' => 0);
		}

		return array('amount' => $total, 'sv_amount' => $svTotal);
	}

	function getOrderItems($id, $itemStatuses)
	{
		// invalid format for item statuses
		if (!is_array($itemStatuses)) return false;

		$results = $this->api->OrdDetail_Get($id);
		$items = array();

		if ($results < 1) return false;

		try {
			$xmlStr = $this->UTF8ForXML($this->api->XML);
			$xml = new SimpleXMLElement($xmlStr);

			foreach ($xml->sqlresult as $result) {
				// item found
				$status = $result->item_state->__toString();

				if (in_array($status, $itemStatuses)) {
					$amount = (float)$result->it_unlist->__toString();
					$discount = (int)$result->discount->__toString();

					$qty = (float)$result->quantf->__toString();

					if (in_array(PAYMENT_REFUND_ITEM_STATUS, $itemStatuses)) {
						$qty = (float)$result->quanto->__toString();
					}

					$amount = $amount * $qty;

					// reduce amount based on discount
					if ($discount > 0) {
						$amount = $amount * ( 1 - ($discount / 100) );
					}

					// tax
					$taxRate = $this->getTaxRate($result);
					$amount = $amount * ( 1 + ( $taxRate / 100 ) );

					$items[] = array(
						'id'		=> $result->item_id->__toString(),
						'name'		=> $result->item->__toString(),
						'amount'	=> $amount,
						'qty'		=> $qty
					);
				}
			}
		} catch (\Exception $e) {
			return false;
		}

		return $items;
	}

	function UTF8ForXML($string)
	{
		// remove encrypted cc_cid field
		$string = preg_replace('/<cc_cid(.+?)<\/cc_cid>/', '<cc_cid />', $string);

		// remove characters that we can't parse
		return preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
	}

	/**
	 * check if an order has items in the given status
	 *
	 * @param int $orderId MOM order id
	 * @param string $status two letter status code
	 * @return bool
	 */
	public function orderHasItems($orderId, $status)
	{
		$results = $this->api->OrdDetail_Get($orderId);

		// invalid order number, no order items found
		if ($results < 1) return false;

		$xmlStr = $this->api->XML;

		// parse XML
		$xml = new SimpleXMLElement( $xmlStr );

		foreach ($xml->sqlresult as $result) {
			// item found
			if ($result->item_state->__toString() == $status) {
				return true;
			}
		}

		return false;
	}

	/**
	 * send JSON response
	 *
	 * @param mixed $data
	 */
	public function sendJSONResponse($data)
	{
		header('Content-Type: application/json');
		echo json_encode($data);
		exit;
	}
}